package com.jd.dev.mock.client.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 配置信息类
 * Created by hanxu3 on 2018/5/10.
 */
public class MockerConfig {
    public static final Logger logger = LoggerFactory.getLogger(MockerConfig.class);

    //默认的配置文件
    public static final String fileName = "mocker.properties";

    Map<String,String> extConfig = new ConcurrentHashMap<String,String>();

    //开关
    private String on;
    //用户erp
    private String erp;
    //配置erp信息并不好，改用token替代erp，将来扩展非京东用户
    private String token;
    //用户ip地址
    private String ip;
    //mockerServer地址
    private String serverAdress = "http://fastmock.jd.care";
    //本地测试ZK
    private String appZkHost = "192.168.192.48:2181";

    /**
     * 加载扩展属性
     * @param key
     * @param value
     */
    public void addExtConfig(String key,String value){
        extConfig.put(key,value);
    }

    public String getOn() {
        return on;
    }

    public void setOn(String on) {
        this.on = on;
    }

    public String getErp() {
        return erp;
    }

    public void setErp(String erp) {
        this.erp = erp;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getServerAdress() {
        return serverAdress;
    }

    public void setServerAdress(String serverAdress) {
        this.serverAdress = serverAdress;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAppZkHost() {
        return appZkHost;
    }

    public void setAppZkHost(String appZkHost) {
        this.appZkHost = appZkHost;
    }
}
