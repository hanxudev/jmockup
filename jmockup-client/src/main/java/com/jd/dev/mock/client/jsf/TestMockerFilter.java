package com.jd.dev.mock.client.jsf;

import com.jd.dev.mock.client.MockDataClient;
import com.jd.jsf.gd.config.annotation.AutoActive;
import com.jd.jsf.gd.config.annotation.Extensible;
import com.jd.jsf.gd.filter.AbstractFilter;
import com.jd.jsf.gd.msg.*;
import com.jd.jsf.gd.util.CommonUtils;
import com.jd.jsf.gd.util.RpcContext;
import com.jd.dev.mock.common.jsf.MockerFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by hanxu3 on 2018/3/6.
 */
@Extensible(value = "TestMockerFilter")
@AutoActive(providerSide = false,consumerSide = true)
public class TestMockerFilter extends AbstractFilter {

    private static final Logger logger = LoggerFactory.getLogger(TestMockerFilter.class);

    //发送请求的工具类
    private static MockDataClient mockDataClient = MockDataClient.getInstance();

    @Override
    public ResponseMessage invoke(RequestMessage request) {

        //剔除注册中心接口
        boolean isExclude = this.checkExclude(request);
        if(isExclude){
            return getNext().invoke(request);
        }

        // 可以拿到JSF一些配置里的信息
        Map<String, Object> configContext = super.getConfigContext();

        //返回结果对象
        ResponseMessage response = null;
        if(mockDataClient.isMockOn()){
            //也可以自己构造返回对象
            response = MessageBuilder.buildResponse(request);
        }

        if(mockDataClient.isMockOn()){
            mockDataClient.printClientConfig();
            try{
                Object result = mockDataClient.sendJsfRequest(request);
                response.setResponse(result);
                if(isAsyncRequest(configContext)){
                    //异步调用，模拟使用future返回结果
                    MockerFuture mockerFuture = new MockerFuture(response);
                    ResponseFuture responseFuture = new ResponseFuture(mockerFuture);
                    RpcContext rpcContext = RpcContext.getContext();
                    rpcContext.setFuture(responseFuture);
                }
            }catch (Exception e){
                logger.error("hessianClient.sendJsfRequest error:",e);
                //附加异常信息
                response.setException(e);
            }
        }else{
            // 在getNext().invoke(request)前加的代码，将在远程方法调用前执行
            response = getNext().invoke(request); // 调用链自动往下层执行
            // 在getNext().invoke(request)后加的代码，将在远程方法调用后执行
        }
        return response;
    }

    /**
     *  过滤掉注册接口
     * @param requestMessage
     * @return
     */
    private boolean checkExclude(RequestMessage requestMessage){
        String clazzName = requestMessage.getClassName();
        if("com.jd.jsf.service.RegistryService".equals(clazzName)){
            return true;
        }
        return false;
    }

    /**
     * 是否是异步请求
     * @param configContext
     * @return
     */
    private boolean isAsyncRequest(Map<String, Object> configContext){
        Boolean async = (Boolean) configContext.get("async");
        return CommonUtils.isTrue(async);
    }
}
