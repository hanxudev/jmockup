package com.jd.dev.mock.client.hessian;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class HessianWithContextProxyFactoryBean extends HessianProxyFactoryBean {
  
    protected HessianWithContextProxyFactory proxyFactory = new HessianWithContextProxyFactory();
  
    protected Map<String,String> headers = new HashMap<String,String>();
  
    public void setHeaders(Map<String, String> headers) {  
        this.headers = headers;  
    }

    public void addHeaders(String key,String value) {
        this.headers.put(key,value);
    }
  
    private String localIp;
  
    public HessianWithContextProxyFactoryBean() {  
        super();  
        super.setProxyFactory(proxyFactory);//强制修改  
    }  
  
    @Override  
    public void afterPropertiesSet() {  
        super.afterPropertiesSet();  
        localIp = getLocalIp();//本地IP地址  
        headers.put(HessianHeaderContext.SOURCE_HOST,localIp);

        proxyFactory.getGlobalHeaders().putAll(headers);//append  
    }  
  
    @Override  
    public Object invoke(MethodInvocation invocation) throws Throwable {
        //all the methods of hessian instances will be invoked here,  
        //so we cant use threadLocal there.  
        return super.invoke(invocation);  
    }  
  
    public String getLocalIp(){
        try {  
            //一个主机有多个网络接口  
            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
            while (netInterfaces.hasMoreElements()) {
                NetworkInterface netInterface = netInterfaces.nextElement();  
                Enumeration<InetAddress> addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress address = addresses.nextElement();  
                    if (address.isSiteLocalAddress() && !address.isLoopbackAddress()) {  
                         return address.getHostAddress();
                    }  
                }  
            }  
        }catch (Exception e) {  
            //  
        }  
        return "";
    }  
}