package com.jd.dev.mock.client;

import com.jd.dev.mock.client.conf.ConfigFactory;
import com.jd.dev.mock.client.hessian.HessianClient;
import com.jd.fastjson.JSON;
import com.jd.jsf.gd.msg.RequestMessage;
import com.jd.jsf.gd.util.*;
import com.jd.dev.mock.client.conf.MockerConfig;
import com.jd.dev.mock.common.jsf.RequestExtend;
import com.jd.dev.mock.common.tools.MapBeanUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by hanxu3 on 2018/3/8.
 */
public class MockDataClient {
    private final static Logger logger = LoggerFactory.getLogger(MockDataClient.class);
    /**
     * 本地配置项
     */
    MockerConfig mockerConfig;

    /**
     * hessian接口
     */
    HessianClient hessianClient;

    /**
     * ip校验结果
     */
    private Boolean checkIp = null;


    public MockDataClient(){
        try{
            init();
        }catch (Exception e){
            logger.error("MockDataClient init error:",e);
        }
    }

    /**
     * 初始化关键组件
     */
    private void init(){
        //加载本地配置文件
        mockerConfig = ConfigFactory.loadDefaultMockerConfig();
        //初始化hessianClient
        //增加开关标识判断
        if(hessianClient == null){
            hessianClient = new HessianClient(mockerConfig);
        }
    }

    /**
     * mock开关
     * @return
     */
    public boolean isMockOn(){
        if(StringUtils.isNotBlank(mockerConfig.getOn())
                && "1".equals(mockerConfig.getOn())){
            return checkIp();
        }
        return false;
    }

    /**
     * 校验本机ip是否为测试ip（即非线上ip）
     * @return
     */
    public boolean checkIp(){
        if(checkIp == null){
            String ip = getLocalHostIP();
            //调接口校验
            checkIp = hessianClient.checkIp(ip);
        }
        return checkIp;
    }

    /**
     * 获取本机的IP
     * @return Ip地址
     */
    public String getLocalHostIP() {
        String ip = "";
        try {
            /**返回本地主机。*/
            InetAddress addr = InetAddress.getLocalHost();
            /**返回 IP 地址字符串（以文本表现形式）*/
            ip = addr.getHostAddress();
        } catch(Exception ex) {
        }
        return ip;
    }

    /**
     * 请求jsf接口
     * @param request
     * @return
     */
    public Object sendJsfRequest(RequestMessage request){
        logger.debug(request.getMethodName() + " RequestMessage:"+JSON.toJSONString(request));
        Class reType = getReturnType(request);
        //便于统一处理，扩展信息放到header中
        RequestExtend extend = new RequestExtend();
        Object tempResult = hessianClient.sendJsfRequest(request,extend);
        logger.debug(request.getMethodName() + " Result:"+JSON.toJSONString(tempResult));
        if(tempResult == null){
            return null;
        }

        Object response = null;//返回结果对象
        try{
            //尝试使用JSf的工具类解析
            response = PojoUtils.realize(tempResult,reType,null);
            if(response != null){
                return response;
            }
        }catch (Exception e){
            logger.error("PojoUtils.realize error:",e);
        }

        //自定义解析数据
        //如果返回的是个集合（暂未确定是否有该类型返回）
        if(tempResult instanceof Collection){
            try{
                //使用BeanUtils解析List到返回对象
                response = MapBeanUtil.parseListBean((List)tempResult);
                logger.debug(JSON.toJSONString(response));
                return response;
            }catch (Exception e){
                logger.error("BeanUtils.populate List Data error:",e);
            }

        }
        //对map返回结果尝试json解析
        if(tempResult instanceof Map){
            try{
                //使用BeanUtils解析map到返回对象
                response = MapBeanUtil.parseMapBean((Map) tempResult);
                return response;
            }catch (Exception e){
                logger.error("BeanUtils.populate Map Data error:",e);
            }
        }
        return response;
    }

    /**
     * 匹配接口，获取接口返回值类型
     * @param  request 请求jsf接口的参数对象
     * @return Class
     */
    private Class getReturnType(RequestMessage request){
        Class returnType = null;
        try {
            String[] argsType = request.getInvocationBody().getArgsType();
            Method method = ReflectUtils.getMethod(request.getClassName(),request.getMethodName(),argsType);
            return method.getReturnType();
        } catch (Exception e) {
            logger.error("getReturnType error:",e);
        }
        try {
            //尝试按名称查找
            Class clz = ClassLoaderUtils.forName(request.getClassName());
            Method[] methods = clz.getMethods();
            for(Method method : methods){
                if(request.getMethodName().equals(method.getName())){
                    returnType = method.getReturnType();
                }
            }
        } catch (ClassNotFoundException e) {
            logger.error("getReturnType from request error:",e);
        }
        return returnType;
    }

    /**
     * 打印配置信息
     */
    public void printClientConfig(){
        if(logger.isDebugEnabled()){
            logger.error(JSON.toJSONString(mockerConfig));
        }
    }

    /**
     * 获取实例
     */
    public static MockDataClient getInstance(){
        return MockDataClientFactory.getInstance();
    }
    private static class MockDataClientFactory{
        private final static MockDataClient client = new MockDataClient();

        public static MockDataClient getInstance(){
            return client;
        }
    }
}
