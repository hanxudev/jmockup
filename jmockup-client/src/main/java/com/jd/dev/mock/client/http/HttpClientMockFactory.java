package com.jd.dev.mock.client.http;

import com.jd.dev.mock.client.conf.ConfigFactory;
import com.jd.dev.mock.client.conf.MockerConfig;
import com.jd.dev.mock.common.tools.RegexUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.conn.NHttpClientConnectionManager;
import org.apache.http.nio.reactor.IOReactorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by hanxu3 on 2018/7/20.
 */
public class HttpClientMockFactory {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientMockFactory.class);

    static CloseableHttpAsyncClient closeableHttpClient ;

    //一些默认参数
    static ConnectionKeepAliveStrategy connectionKeepAliveStrategy = new DefaultConnectionKeepAliveStrategy();
    static CookieStore basicCookieStore = new BasicCookieStore();

    /**
     * 获取一个支持mock的client
     */
    public static CloseableHttpAsyncClient getAsyncHttpMockClient() throws IOReactorException{
        if(closeableHttpClient == null){
            synchronized (HttpClientMockFactory.class){
                if(closeableHttpClient == null){
                    closeableHttpClient =  makeClientObject();
                    closeableHttpClient.start();
                }
            }
        }
        return closeableHttpClient;
    }

    /**
     * client生成器
     * @return
     */
    private static CloseableHttpAsyncClient makeClientObject() throws IOReactorException {
        // Create an HttpClient with the given custom dependencies and configuration.
        return makeAsyncClientObject(false,connectionKeepAliveStrategy, null, basicCookieStore);
    }

    /**
     * client生成器
     * @param redirect  bool,是否支持转发
     * @param keepAliveStrategy 长连接策略 可为null
     * @param connectionManager 连接管理器  可为null
     * @param basicCookieStore cookie存储器 可为null
     * @return
     */
    public static CloseableHttpAsyncClient makeAsyncClientObject(final boolean redirect,
                                                                 ConnectionKeepAliveStrategy keepAliveStrategy,
                                                                 NHttpClientConnectionManager connectionManager,
                                                                 CookieStore basicCookieStore){
        // Create an HttpClient with the given custom dependencies and configuration.
        HttpAsyncClientBuilder builder =  HttpAsyncClients.custom()
                .setDefaultRequestConfig(// Create global request configuration
                        RequestConfig.custom()
                                .setExpectContinueEnabled(true)
                                .setRedirectsEnabled(redirect)
                                .setRelativeRedirectsAllowed(redirect)
                                .setCookieSpec(CookieSpecs.DEFAULT)
                                .build()
                )
                //这些参数将来都要放开，支持自定义
                //todo
                .setKeepAliveStrategy(keepAliveStrategy)
                .setConnectionReuseStrategy(new DefaultConnectionReuseStrategy())
                .setRedirectStrategy(new DefaultRedirectStrategy())
                .addInterceptorFirst(new MockRequestInterceptor())
                .setProxy(getDefaultMockProxy())
                ;
        if(connectionManager != null){
            builder.setConnectionManager(connectionManager);
        }
        if(basicCookieStore != null){
            //Use custom cookie store if necessary
            builder.setDefaultCookieStore(basicCookieStore);
        }
        return builder.build();
    }

    /**
     * 获取代理
     */
    private static HttpHost getDefaultMockProxy(){
        MockerConfig config = ConfigFactory.loadDefaultMockerConfig();
        String url = config.getServerAdress();
        String scheme = null;
        String hostName = url;
        String port = null;
        int portInt = 80;
        if(StringUtils.isNotBlank(url)){
            try{
                int i = url.indexOf("://");
                if(i > 0){
                    scheme = url.substring(0,i);
                }
                String temp = url.substring(i + 3);
                if(temp.contains(":")){
                    int j = temp.indexOf(":");
                    hostName = temp.substring(0,j);
                    port = temp.substring(j+1);
                    int m = port.indexOf("/");
                    if(m > 0){
                        port = port.substring(0,m);
                    }
                    if(StringUtils.isNotBlank(port)){
                        portInt = Integer.parseInt(port);
                    }
                }else  if(temp.contains("/")){
                    int p = temp.indexOf("/");
                    hostName = temp.substring(0,p);
                }else{
                    hostName = temp;
                }
            }catch (Exception e){
                logger.error("init httpProxy error:",e);
            }
        }
        HttpHost httpHost = null;
        if(RegexUtils.isIP(hostName)){
            try {
                InetAddress inetAddress = InetAddress.getByName(hostName);
                httpHost = new HttpHost(inetAddress,portInt,scheme);
            } catch (UnknownHostException e) {
                logger.error("new httpHost error:",e);
            }
        }
        if(httpHost == null){
            httpHost = new HttpHost(hostName,portInt,scheme);
        }
        return httpHost;
    }

}
