package com.jd.dev.mock.client.conf;

import com.alibaba.fastjson.JSONObject;
import com.jd.dev.mock.common.ResultConstant;
import com.jd.dev.mock.common.tools.SimpleHttpUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 读取本地配置文件
 * Created by hanxu3 on 2018/5/10.
 */
public class ConfigFactory {
    private static final Logger logger = LoggerFactory.getLogger(MockerConfig.class);

    private static MockerConfig defaultMockConfig = null;

    /**
     * 拉取默认配置接口
     */
    private static final String MOCK_CONFIG_URL = "mocker/config";

    /**
     * 拉取zk配置接口
     */
    private static final String MOCK_CONFIG_ZK_URL = "mocker/config/zk";

    /**
     * 读取配置文件
     * @param fileName
     * @return
     */
    public static MockerConfig loadMockerConfig(String fileName){
        Properties prop = null;
        try {
            prop = PropertiesLoaderUtils.loadAllProperties(fileName);
        }catch (Exception e){
            logger.error("load config error:",e);
        }
        MockerConfig config = new MockerConfig();
        if(prop != null){
            try{
                config.setOn(prop.getProperty(ConfigConstant.Mocker_Switch,""));
                config.setServerAdress(prop.getProperty(ConfigConstant.Mocker_Server_Url));
                if(StringUtils.isBlank(config.getServerAdress())){
                    logger.error(ConfigConstant.Mocker_Server_Url +" can not be null");
                }
                config.setErp(prop.getProperty(ConfigConstant.Mocker_User));
                config.setToken(prop.getProperty(ConfigConstant.Mocker_Token));
                config.setAppZkHost(prop.getProperty(ConfigConstant.Mocker_ZkConnection));
            }catch (Exception e){
                logger.error("Build MockerConfig error:",e);
            }
        }
        try{
            loadRemoteConfig(config);
        }catch (Exception e){
            logger.error("loadRemoteConfig error:",e);
        }
        return config;
    }

    /**
     * 加载远程配置
     */
    private static void loadRemoteConfig(MockerConfig config){
        String host = config.getServerAdress();
        if(StringUtils.isNotBlank(host)){
            StringBuilder reqUrl = getRequestUrl(host,MOCK_CONFIG_URL);
            try {
                String json = SimpleHttpUtils.get(reqUrl.toString());
                if(StringUtils.isNotBlank(json)){
                    HashMap<String,String> jsonMap = JSONObject.parseObject(json, HashMap.class);
                    for(Map.Entry<String,String> entry : jsonMap.entrySet()){
                        if(ResultConstant.APP_ZK_HOST.equals(entry.getKey())
                                && StringUtils.isBlank(config.getAppZkHost())
                                && StringUtils.isNotBlank(entry.getValue())){
                            config.setAppZkHost(entry.getValue());
                        }
                        config.addExtConfig(entry.getKey(),entry.getValue());
                    }
                }
            } catch (Exception e) {
                logger.error("loadRemoteConfig error:",e);
            }
        }
    }

    /**
     * Mocker默认的配置
     * @return
     */
    public static MockerConfig loadDefaultMockerConfig(){
        if(defaultMockConfig == null){
            defaultMockConfig = loadMockerConfig(MockerConfig.fileName);
        }
        return defaultMockConfig;
    }

    /**
     * 获取zk的api地址
     * @return
     */
    public static StringBuilder getZkRequestUrl(){
        String host = loadDefaultMockerConfig().getServerAdress();
        return getRequestUrl(host,MOCK_CONFIG_ZK_URL);
    }

    /**
     * 请求的URL
     * @param host
     * @param configUrl
     * @return
     */
    private static StringBuilder getRequestUrl(String host,String configUrl){
        StringBuilder reqUrl = new StringBuilder(host);
        if(StringUtils.isNotBlank(host)) {

            if (!host.endsWith("/")) {
                reqUrl.append("/");
            }
            reqUrl.append(configUrl);
        }
        return reqUrl;
    }
}
