package com.jd.dev.mock.client.register;

import com.jd.dev.mock.common.tools.RegexUtils;
import com.jd.jsf.gd.config.RegistryConfig;
import com.jd.jsf.gd.config.spring.ConsumerBean;
import com.jd.jsf.gd.config.spring.ConsumerGroupBean;
import com.jd.jsf.gd.util.Constants;
import com.jd.jsf.gd.util.JSFContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * Created by hanxu3 on 2018/8/15.
 */
public class JsfMockRegister implements BeanPostProcessor {
    private boolean mock = true;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (!isMock()) {
            return bean;
        }

        //取消Jsf强依赖服务端
        if (bean.getClass() == ConsumerBean.class) {
            ((ConsumerBean)bean).setCheck(false);
            ((ConsumerBean)bean).setLazy(true);
        }else if(bean.getClass() == ConsumerGroupBean.class){
            ((ConsumerGroupBean)bean).setCheck(false);
            ((ConsumerGroupBean)bean).setLazy(true);
        }else if(bean instanceof RegistryConfig){
            if(!RegexUtils.isOSLinux()){
                //非Linux设置epoll为false
                JSFContext.putGlobalVal(Constants.SETTING_TRANSPORT_CONSUMER_EPOLL,"false");
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    public boolean isMock() {
        return mock;
    }

    public void setMock(boolean mock) {
        this.mock = mock;
    }
}
