package com.jd.dev.mock.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockerClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockerClientApplication.class, args);
	}
}
