package com.jd.dev.mock.client.http;

import com.jd.dev.mock.client.conf.ConfigFactory;
import com.jd.dev.mock.client.conf.MockerConfig;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;

/**
 * 自动添加mocker的header
 * Created by hanxu3 on 2018/7/20.
 */
public class MockRequestInterceptor implements HttpRequestInterceptor {
    @Override
    public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
        MockerConfig mockerConfig = ConfigFactory.loadDefaultMockerConfig();
        httpRequest.addHeader("mocker","1");
        httpRequest.addHeader("token",mockerConfig.getToken());
    }
}
