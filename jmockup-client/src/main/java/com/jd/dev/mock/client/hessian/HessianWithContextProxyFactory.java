package com.jd.dev.mock.client.hessian;

import com.caucho.hessian.client.HessianProxyFactory;
import com.caucho.hessian.io.HessianRemoteObject;

import java.lang.reflect.InvocationHandler;

import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HessianWithContextProxyFactory extends HessianProxyFactory {
  
    private Map<String,String> globalHeaders = new HashMap<String,String>();
  
    public HessianWithContextProxyFactory() {  
        super();  
    }  
  
    public void setGlobalHeaders(Map<String, String> globalHeaders) {  
        this.globalHeaders = globalHeaders;  
    }  
  
    public Map<String, String> getGlobalHeaders() {  
        return globalHeaders;  
    }

    @Override
    public Object create(Class<?> api, URL url, ClassLoader loader)
    {
        if (api == null) {
            throw new NullPointerException("api must not be null for HessianProxyFactory.create()");
        }

        InvocationHandler handler = new HessianWithContextProxy(url, this, api,globalHeaders);
        return Proxy.newProxyInstance(loader,
                new Class[]{api, HessianRemoteObject.class},
                handler);
    }
}