package com.jd.dev.mock.client.conf;

/**
 * 配置key
 * Created by hanxu3 on 2018/5/10.
 */
public class ConfigConstant {
    /**
     * mock功能开关
     */
    public static final String Mocker_Switch = "mocker.on";
    /**
     * mocker的服务地址
     */
    public static final String Mocker_Server_Url = "mocker.url";

    /**
     * 用户erp
     */
    public static final String Mocker_User = "mocker.erp";

    /**
     * 用户token
     */
    public static final String Mocker_Token = "mocker.token";

    /**
     * 配置本地的zk地址
     */
    public static final String Mocker_ZkConnection = "mocker.zk";

    /**
     * 分隔符
     */
    public static final String MULTI_WATCH_PATH = ";";
}

