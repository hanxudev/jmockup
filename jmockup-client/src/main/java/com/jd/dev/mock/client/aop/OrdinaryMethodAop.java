package com.jd.dev.mock.client.aop;

import com.alibaba.fastjson.JSON;
import com.jd.dev.mock.client.conf.ConfigFactory;
import com.jd.dev.mock.client.conf.MockerConfig;
import com.jd.dev.mock.common.tools.SimpleHttpUtils;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;

import java.lang.reflect.Method;
import java.util.Map;

public class OrdinaryMethodAop implements MethodInterceptor {
    private final static Log log = LogFactory.getLog(OrdinaryMethodAop.class);

    private static final String MOCK_API = "mocker/method/data?";

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        try {
            Method method = invocation.getMethod();
            Object[] args = invocation.getArguments();
            String className = method.getDeclaringClass().getName();
            String methodName = method.getName();
            String returnType = method.getReturnType().getName();
            return doMockInvoke(className,methodName,method.getParameterTypes(),returnType);
        }catch (Exception e){
            log.error("OrdinaryMethodAop.invoke  error:",e);
        }
        return null;
    }

    public Object doAroundMethod(ProceedingJoinPoint pjp){
        try{
            Class clazz = pjp.getTarget().getClass(); // 代理对象
            String clazzName = clazz.getName();
            Method[] methods = clazz.getMethods(); // 代理对象包含的方法
            String methodName = pjp.getSignature().getName(); // 方法名
            Class<?>[] parameterTypes = null;
            String returnType = null; // 方法返回类型

            for(Method method : methods) {
                if(method.getName().equalsIgnoreCase(methodName)) {
                    parameterTypes = method.getParameterTypes(); // 方法参数类型数组
                    returnType = method.getReturnType().getName(); // 方法返回类型
                    log.debug("returnType：" + returnType); // java.util.List
                }
            }
            return doMockInvoke(clazzName,methodName,parameterTypes,returnType);
        }catch (Exception e){
            log.error("OrdinaryMethodAop.doAroundMethod  error:",e);
        }
        return null;
    }

    /**
     * 处理逻辑
     * @param clazzName
     * @param methodName
     * @param parameterTypes
     * @param returnType
     * @return
     */
    private Object doMockInvoke(String clazzName,String methodName,Class<?>[] parameterTypes,String returnType){
        String params = ""; // 拼接参数类型
        // 构造参数类型数组
        for(int i = 0; i < parameterTypes.length; i++) {
            params += parameterTypes[i].getName() + ",";
        }
        params = params.substring(0, params.length() - 1);

        MockerConfig config = ConfigFactory.loadDefaultMockerConfig();
        StringBuilder reqUrl = new StringBuilder();
        reqUrl.append(config.getServerAdress());
        if(!config.getServerAdress().endsWith("/")){
            reqUrl.append("/");
        }
        reqUrl.append(MOCK_API);
        reqUrl.append("token=").append(config.getToken());
        reqUrl.append("&localClassName=").append(clazzName);
        reqUrl.append("&localMethodName=").append(methodName);
        reqUrl.append("&args=").append(params);

        try {
            String result = SimpleHttpUtils.get(reqUrl.toString());
            Map resultMap = JSON.parseObject(result, Map.class);
            log.debug("mkLog ==> 调mock server返回结果：" + JSON.toJSONString(resultMap));
            String status = (String) resultMap.get("status");
            String data = (String) resultMap.get("returnData");
            String returnTypeForMockServer = (String) resultMap.get("returnType");
            if(!StringUtils.isEmpty(returnTypeForMockServer)) {
                returnType = returnTypeForMockServer;
            }
            if("success".equalsIgnoreCase(status)) {
                if(!StringUtils.isEmpty(data)) {
                    try {
                        return JSON.parseObject(data, Class.forName(returnType));
                    } catch (ClassNotFoundException e) {
                        log.error("error：" + e);
                    }
                }
            }
        } catch (Exception e) {
            log.error("doMockInvoke error:",e);
        }
        return null;
    }
}
