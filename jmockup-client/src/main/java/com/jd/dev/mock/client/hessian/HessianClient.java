package com.jd.dev.mock.client.hessian;

import com.jd.jsf.gd.msg.Invocation;
import com.jd.jsf.gd.msg.RequestMessage;
import com.jd.jsf.gd.util.PojoUtils;
import com.jd.dev.mock.client.conf.MockerConfig;
import com.jd.dev.mock.common.jsf.JsfServiceInfo;
import com.jd.dev.mock.common.jsf.RequestExtend;
import com.jd.dev.mock.common.api.JsfServiceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


/**
 * 请求hessian接口工具类
 * Created by hanxu3 on 2018/5/14.
 */
public class HessianClient {
    private final static Logger logger = LoggerFactory.getLogger(HessianClient.class);

    /**
     * 保存接口代理对象
     */
    private JsfServiceAdapter adapterService;

    //hessian接口地址
    private final String hessianUrl = "hessian/JsfServiceAdapter";

    public HessianClient(MockerConfig mockerConfig){
        String serviceUrl = null;//接口地址
        String serverUrl = mockerConfig.getServerAdress();
        if (serverUrl.endsWith("/")){
            serviceUrl = serverUrl.concat(hessianUrl);
        }else{
            serviceUrl = serverUrl.concat("/").concat(hessianUrl);
        }
        //创建服务
        HessianWithContextProxyFactoryBean hessianProxyFactoryBean = new HessianWithContextProxyFactoryBean();
        hessianProxyFactoryBean.setServiceUrl(serviceUrl);
        hessianProxyFactoryBean.setServiceInterface(JsfServiceAdapter.class);
        //设置全局的headers
        hessianProxyFactoryBean.addHeaders("token",mockerConfig.getToken());
        hessianProxyFactoryBean.afterPropertiesSet();//真正的创建代理对象
        adapterService = (JsfServiceAdapter)hessianProxyFactoryBean.getObject();
    }

    /**
     * 发送请求
     * @param requestMessage
     * @return
     */
    public Object sendJsfRequest(RequestMessage requestMessage,RequestExtend extend){
        if(extend != null){
            extend.setReceiveTime(requestMessage.getReceiveTime());
            extend.setTargetAddress(requestMessage.getTargetAddress());
        }

        //请求信息
        JsfServiceInfo jsfServiceInfo = new JsfServiceInfo();
        jsfServiceInfo.setClazzName(requestMessage.getClassName());
        jsfServiceInfo.setMethodName(requestMessage.getMethodName());
        jsfServiceInfo.setAlias(requestMessage.getAlias());
        Invocation invocation = requestMessage.getInvocationBody();
        if(invocation != null){
//            String token = (String)invocation.getAttachment(Constants.HIDDEN_KEY_TOKEN);
//            if(StringUtils.isNotBlank(token)){
//                jsfServiceInfo.setToken(token);
//            }
            //attachments 包含token
            Map<String,Object> attachments = invocation.getAttachments();
            if(attachments != null && !attachments.isEmpty()){
                for(Map.Entry<String,Object> entry : attachments.entrySet()){
                    jsfServiceInfo.addParameter(entry.getKey(),(String)entry.getValue());
                }
            }

            //组织参数类型
            String[] argsType = requestMessage.getInvocationBody().getArgsType();
            if(argsType != null && argsType.length > 0){
                jsfServiceInfo.setArgsType(argsType);
                //组织参数
                Object[] args = invocation.getArgs();
                //自定义类型转成map结构
                Object[] params = PojoUtils.generalize(args);
                jsfServiceInfo.setArgs(params);
            }
        }
        return adapterService.doJsfRequest(jsfServiceInfo,extend);
    }

    /**
     * 校验ip
     * @param ip
     * @return
     */
    public boolean checkIp(String ip){
        try {
            return adapterService.checkIp(ip);
        }catch (Exception e){
            logger.error("checkIp  error:",e);
        }
        return false;
    }

}
