package com.jd.dev.mock.client.hessian;

import com.caucho.hessian.client.HessianConnection;
import com.caucho.hessian.client.HessianProxy;
import com.caucho.hessian.client.HessianProxyFactory;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HessianWithContextProxy extends HessianProxy {

    protected Map<String,String> globalHeaders = new HashMap<String,String>();
  
    public HessianWithContextProxy(URL url, HessianProxyFactory factory) {
        super(url, factory);
    }  
  
    public HessianWithContextProxy(URL url, HessianProxyFactory factory, Class type) {  
        super(url, factory, type);
    }

    public HessianWithContextProxy(URL url, HessianProxyFactory factory, Class type,Map<String,String> headers) {
        super(url, factory, type);
        globalHeaders = headers;
    }

    @Override
    protected void addRequestHeaders(HessianConnection conn){
        super.addRequestHeaders(conn);
        // add Hessian global Header;
        for (Map.Entry<String, String> entry : globalHeaders.entrySet()) {
            conn.addHeader(entry.getKey(), entry.getValue());
        }
        // add Hessian Header
        Map<String, String> headerMap = HessianHeaderContext.getContext().getHeaders();
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            conn.addHeader(entry.getKey(), entry.getValue());
        }
    }
}