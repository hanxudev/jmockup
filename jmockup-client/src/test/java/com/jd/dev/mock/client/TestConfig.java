package com.jd.dev.mock.client;

import com.jd.fastjson.JSON;
import com.jd.dev.mock.common.tools.MapBeanUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by hanxu3 on 2018/5/14.
 */
@RunWith(SpringRunner.class)
public class TestConfig {

    @Test
    public void testConfigFile(){
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("mocker.properties");
            String mockerOn = properties.getProperty("mocker.on");
            System.out.println(mockerOn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMapType(){
        Map source = new ConcurrentHashMap();
        System.out.println(source instanceof Map);
        System.out.println(source instanceof ConcurrentHashMap);

    }

    @Test
    public void testMapToBean(){
        String jsonMap = "{\"symbol\":\"￥\",\"defaultFractionDigits\":2,\"currencyCode\":\"CNY\",\"class\":\"java.util.Currency\"}";
        String json = "{\"friehtPattern\":2,\"venderId\":215125,\"class\":\"com.jd.freight.platform.server.account.entity.ShoppingCartFare\"}";

        HashMap data = JSON.parseObject(json, HashMap.class);
        Object obj = MapBeanUtil.parseMapBean(data);
        System.out.println(JSON.toJSONString(obj));
//        System.out.println(((Currency)obj).getSymbol());
    }
}
