package com.jd.dev.mock.common.jsf;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * jsf请求接口信息
 * Created by hanxu3 on 2018/5/11.
 */
public class JsfServiceInfo implements Serializable {

    //service名称
    private String clazzName;
    //方法名称
    private String methodName;
    //分组
    private String alias;
    //token验证码
    String token;
    //token和source等jsf参数统一放到map里
    private Map<String,String> parameters = new HashMap<String, String>(4);
    //参数类型
    private String[] argsType;
    //参数列表
    //泛化调用jsf接口，这个参数列表如果某个参数是自定义类型，这里其实是hashMap结构
    private Object[] args;

    private transient Class[] argClasses;

    //指定IP请求
    String targetAddress;
    //返回值类型
    String returnType;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getClazzName() {
        return clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    public String[] getArgsType() {
        return argsType;
    }

    public void setArgsType(String[] argsType) {
        this.argsType = argsType;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public String getTargetAddress() {
        return targetAddress;
    }

    public void setTargetAddress(String targetAddress) {
        this.targetAddress = targetAddress;
    }

    public Class[] getArgClasses() {
        return argClasses;
    }

    public void setArgClasses(Class[] argClasses) {
        this.argClasses = argClasses;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public void addParameter(String key,String value){
        this.parameters.put(key,value);
    }
}
