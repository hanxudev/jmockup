package com.jd.dev.mock.common.jsf;

import java.io.Serializable;

/**
 * jsf请求的基本扩展信息
 * Created by hanxu3 on 2018/5/15.
 */
public class RequestExtend implements Serializable{

    private long receiveTime;
    private String targetAddress;

    public long getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(long receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getTargetAddress() {
        return targetAddress;
    }

    public void setTargetAddress(String targetAddress) {
        this.targetAddress = targetAddress;
    }
}
