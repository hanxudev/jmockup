package com.jd.dev.mock.common.mock;

import java.io.Serializable;

/**
 * Created by hanxu3 on 2018/5/11.
 */
public class MockerResponse implements Serializable{
    //结果码
    int code;
    String errorMsg;
    //结果数据
    String result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
