package com.jd.dev.mock.common.mock;

import com.jd.dev.mock.common.jsf.JsfServiceInfo;

import java.io.Serializable;

/**
 * 发送给mocker的请求参数
 * Created by hanxu3 on 2018/5/11.
 */
public class MockerRequest implements Serializable{
    //基础信息
    MockerBaseInfo baseInfo;
    //请求的JSF接口信息
    JsfServiceInfo jsfServiceInfo;

    public JsfServiceInfo getJsfServiceInfo() {
        return jsfServiceInfo;
    }

    public void setJsfServiceInfo(JsfServiceInfo jsfServiceInfo) {
        this.jsfServiceInfo = jsfServiceInfo;
    }

    public MockerBaseInfo getBaseInfo() {
        if(baseInfo == null){
            baseInfo = new MockerBaseInfo();
        }
        return baseInfo;
    }

    public void setBaseInfo(MockerBaseInfo baseInfo) {
        this.baseInfo = baseInfo;
    }

}
