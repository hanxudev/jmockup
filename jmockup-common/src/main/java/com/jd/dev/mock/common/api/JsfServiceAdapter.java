package com.jd.dev.mock.common.api;

import com.jd.dev.mock.common.jsf.JsfServiceInfo;
import com.jd.dev.mock.common.jsf.RequestExtend;

/**
 * Created by hanxu3 on 2018/5/14.
 */
public interface JsfServiceAdapter {
    /**
     * 代理执行jsf
     * @param invocation
     * @param extend
     * @return
     */
    public Object doJsfRequest(JsfServiceInfo invocation, RequestExtend extend);

    /**
     * 校验ip
     * @param ip
     * @return
     */
    public boolean checkIp(String ip);
}
