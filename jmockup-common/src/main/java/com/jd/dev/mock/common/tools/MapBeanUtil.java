package com.jd.dev.mock.common.tools;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

/**
 * map数据转成java对象
 * Created by hanxu3 on 2018/5/15.
 */
public class MapBeanUtil {

    private final static Logger logger = LoggerFactory.getLogger(MapBeanUtil.class);
    static {
        initBeanConverters();
    }
    /**
     * 初始化转换器
     */
    private static void initBeanConverters(){
        //处理时间格式
        DateConverter dateConverter = new DateConverter(null);
        //设置日期格式
        dateConverter.setPatterns(new String[]{"yyyy-MM-dd","yyyy-MM-dd HH:mm:ss"});
        //注册格式
        ConvertUtils.register(dateConverter, Date.class);
        ConvertUtils.register(dateConverter, String.class);

        //处理BigDecimal
        BigDecimalConverter bigDecimalConverter = new BigDecimalConverter(null);
        ConvertUtils.register(bigDecimalConverter,BigDecimal.class);
    }

    /**
     * 解析Map成JavaBean
     * @param map
     * @return
     */
    public static Object parseMapBean(Map<String,Object> map){
        Object reObj = null;
        try{
            if(map != null && !map.isEmpty()){
                String clazzName = (String)map.get("class");
                if(StringUtils.isBlank(clazzName)){
                    //没有class标识，按map处理
                    clazzName = "java.util.Map";
                }

                //value属性集合
                Map valsMap = new HashMap();
                for(Map.Entry<String,Object> entry : map.entrySet()){
                    String key = entry.getKey();
                    if(!"class".equals(key)){
                        valsMap.put(key,parseNodeBean(entry.getValue()));
                    }
                }
                //根据values初始化Bean
                Class clz = Class.forName(clazzName);
                try{
                    //初始化实例对象
                    if("java.util.Currency".equals(clazzName)){
                        String curCode = (String)valsMap.get("currencyCode");
                        reObj = Currency.getInstance(curCode);
                    }else if("java.util.Map".equals(clazzName)){
                        reObj = new HashMap<String,Object>(valsMap);
                    }else{
                        reObj = clz.newInstance();
                        BeanUtils.populate(reObj,valsMap);
                    }
                }catch (Exception e){
                    logger.error("BeanUtils.populate error:value={}",JSON.toJSONString(valsMap),e);
                    //尝试json解析
                    if(clz != null){
                        reObj = mapToBeanByJson(valsMap,clz);
                    }
                }
            }
        }catch (Exception e){
            //发生异常，继续执行
            logger.error("convert map to bean error:value{}", JSON.toJSONString(map),e);
        }
        return reObj;
    }

    /**
     * 解析集合类型
     */
    public static List parseListBean(List list){
        List beanList = new ArrayList<Object>();
        if(CollectionUtils.isNotEmpty(list)){
            for(Object a :list){
                Object o = parseNodeBean(a);
                beanList.add(o);
            }
        }
        return beanList;
    }

    /**
     * 解析单节点数据结构
     */
    public static Object parseNodeBean(Object bean){
        Object temp;
        if(bean instanceof Map){
            temp = parseMapBean((Map)bean);
        }else if(bean instanceof List){
            temp = parseListBean((List)bean);
        }else{
            temp = bean;
        }
        return temp;
    }

    /**
     * 使用fastJson转换
     * @param valsMap
     */
    public static <T>T mapToBeanByJson(Map valsMap,Class T){
        //移除class字段
        try{
            valsMap.remove("class");
        }catch (Exception e){
            logger.error("remove class param error:",e);
        }

        String json = JSON.toJSONString(valsMap);
        return (T)JSON.parseObject(json,T.getClass(), Feature.UseBigDecimal);
    }
}
