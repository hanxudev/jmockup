package com.jd.dev.mock.common.mock;

import java.io.Serializable;

/**
 * mocker请求的base信息
 * Created by hanxu3 on 2018/5/11.
 */
public class MockerBaseInfo implements Serializable {
    //用户自己的erp
    String erp;

    //应用名称
    String appName;

    String args;//参数

    String argsType;//参数类型

    //接口描述
    private String methodDesc;

    private String pin;

    private String ip;

    //mock的接口返回数据
    private String resultData;

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getArgsType() {
        return argsType;
    }

    public void setArgsType(String argsType) {
        this.argsType = argsType;
    }

    public String getErp() {
        return erp;
    }

    public void setErp(String erp) {
        this.erp = erp;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getMethodDesc() {
        return methodDesc;
    }

    public void setMethodDesc(String methodDesc) {
        this.methodDesc = methodDesc;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }
}
