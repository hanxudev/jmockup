package com.jd.dev.mock.common.mock;

/**
 * Created by zhangzhao8 on 2018/7/14.
 */
public class MockerMethodRequest {
    //基础信息
    MockerBaseInfo baseInfo;
    private String clazz;
    private String methodName;
    private String mockDataId;

    public MockerBaseInfo getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(MockerBaseInfo baseInfo) {
        this.baseInfo=baseInfo;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz=clazz;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName=methodName;
    }

    public String getMockDataId() {
        return mockDataId;
    }

    public void setMockDataId(String mockDataId) {
        this.mockDataId=mockDataId;
    }
}
