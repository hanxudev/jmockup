package com.jd.dev.mock.common.jsf;

import com.jd.jsf.gd.client.MsgFuture;
import com.jd.jsf.gd.msg.ResponseMessage;

import java.util.concurrent.TimeUnit;

/**
 * 继承jsf的MsgFuture，模拟异步调用返回
 * Created by hanxu3 on 2018/5/16.
 */
public class MockerFuture extends MsgFuture {

    ResponseMessage response = null;

    public MockerFuture(ResponseMessage result){
        super(null,null,500);
        response = result;
    }

    @Override
    public Object get(long timeout, TimeUnit unit) throws InterruptedException {
        return response;
    }

    @Override
    public Object get() throws InterruptedException {
        return response;
    }

    @Override
    public MsgFuture setSuccess(Object result) {
        return super.setSuccess(result);
    }
}
