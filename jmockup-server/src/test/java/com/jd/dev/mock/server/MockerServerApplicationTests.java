package com.jd.dev.mock.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MockerServerApplicationTests {

	@Test
	public void contextLoads() {

		String str = " 7天内退货，15天内换货，可获得%s运费赔付(到小金库)";
		System.out.println(String.format(str, "10元"));

        BigDecimal b1 = new BigDecimal(0);
        System.out.println(b1.equals(BigDecimal.ZERO));


    }

	@Test
	public void testIp(){
		InetAddress addr = null;
		try {
			addr = InetAddress.getLocalHost();
			String ip = addr.getHostAddress();
			System.out.println(ip);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}

}
