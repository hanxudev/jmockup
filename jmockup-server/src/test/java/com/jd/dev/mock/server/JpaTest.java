package com.jd.dev.mock.server;

import com.alibaba.fastjson.JSON;
import com.jd.dev.mock.server.domain.MockMethodData;
import com.jd.dev.mock.server.dto.MethodQueryRequest;
import com.jd.dev.mock.server.dao.mongodb.MockMethodRepository;
import com.jd.dev.mock.server.service.MockMethodService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangzhao8 on 2018/7/14.
 */
@SpringBootTest(classes = {MockerServerApplication.class})
@RunWith(SpringRunner.class)
public class JpaTest {
    @Autowired
    private MockMethodRepository mockMethodRepository;
    @Autowired
    private MockMethodService mockMethodService;

    //@Test
    /*public void findByErpAndMethodName() throws Exception {
      //  System.out.println("################################"+JSON.toJSON(mockMethodRepository.findByErpAndMethodName("zhangzhao88","com.jd.m.mocker.server.test.TestClass.test")));
    }*/
    @Test
    public void query(){
        List<String> args=new ArrayList<>();
        args.add("com.lang.String");
        args.add("com.lang.String2");
        MethodQueryRequest queryRequest=new MethodQueryRequest("","org.zhangzhao","testA",args);
        MockMethodData MockMethodData=mockMethodService.mockData(queryRequest);
        System.out.println("################################"+JSON.toJSON(MockMethodData));
    }
   /* @Test
    public void findById(){
        MockMethodData map=mockMethodService.getById("5b52e31396d59e026075a22c");
        System.out.println("################################"+JSON.toJSON(map));
    }*/
}

