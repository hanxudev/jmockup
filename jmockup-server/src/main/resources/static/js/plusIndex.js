(function(){
    //触屏轮图片的方法

    function scroll() {
        return {
            top: window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0,
            left: window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0
        };
    };
    //触屏轮图片的方法
    function banner() {
        var box = document.querySelector('.carousel');

        //定变量 存储触屏 数据
        var startX = 0;
        var moveX = 0;
        var distanceX = 0;
        //绑定触屏事件
        box.addEventListener('touchstart', function (e) {
            // 记录触屏开始的数据
            startX = e.targetTouches[0].clientX;
        })

        box.addEventListener('touchmove', function (e) {
            // 记录触屏移动的数据
            moveX = e.targetTouches[0].clientX;
            //算出距离差
            distanceX = moveX - startX;
        })

        box.addEventListener('touchend', function (e) {
            //触屏结束判断用户操作方式
            if (distanceX > 0) {
                //调用框架提供的方法
                $('.carousel').carousel('prev');
            }

            if (distanceX < 0) {
                //调用框架提供的方法
                $('.carousel').carousel('next');
            }

            startX = 0;
            moveX = 0;
            distanceX = 0;

        })
    };
//  动态渲染轮播图图片
    function renderSlider() {
        $.ajax({
            type: 'get',
            dataType: 'json',
            url:'//av.m.jd.care/api/ads/getOne?id=r1MNU2AseM',
            success: function (info) {
                if (info && info.doc.items) {
                    imgTemp(info);
                } else {
                    temp = ' <a class="item active">' +
                        '<img class="carousel-img  " src="./i/banner.png" alt="" />' +
                        '</a>';
                    $('.arrow-l').hide();
                    $('#carousel-img').html(temp);
                    $('.arrow-l').hide();
                }
            },
            error: function (err) {
                temp = ' <a class="item active">' +
                    '<img class="carousel-img  " src="./i/banner.png" alt="" />' +
                    '</a>';
                $('.arrow-l').hide();
                $('#carousel-img').html(temp);
                $('.arrow-l').hide();
                // alert('服务器错误，请稍后重试！');
            }
        })

    };
    //获取图片模板
    function imgTemp(info) {
        var temp = '';
        var tem_p = '';
        var arrow_l=$('.arrow-l');
        if (info.doc.items.length <= 0) {
            temp = ' <a class="item active">' +
                '<img class="carousel-img  " src="./i/banner.png" alt="" />' +
                '</a>';
            arrow_l.hide();
        } else if (info.doc.items.length == 1) {
            temp += '<a class="item active" href="'+judgeLink(info.doc.items[0].link)+'" target="_blank">' +
                ' <img class="carousel-img  " src="' + judgeJson(info.doc.items[0].sImg) + '" alt="" />' +
                '</a>';
            arrow_l.hide();
        } else if (info.doc.items.length > 1) {
            $.each(info.doc.items, function (i, d) {
                if (i == 0) {
                    temp += '<a class="item active" href="'+judgeLink(d.link)+'" target="_blank">' +
                        ' <img class="carousel-img  " src="' + judgeJson(d.sImg) + '" alt="" />' +
                        '</a>';
                    tem_p += ' <li data-target="#carousel-example-generic" data-slide-to="' + (i) + '" class="active"></li>';
                } else {
                    temp += '<a class="item" href="'+judgeLink(d.link)+'" target="_blank">' +
                        ' <img class="carousel-img  " src="' + judgeJson(d.sImg) + '" alt="" />' +
                        '</a>';
                    tem_p += '<li data-target="#carousel-example-generic" data-slide-to="' + (i) + '"></li>'
                }
            })
            arrow_l.show();
        }

        $('#carousel-img').html(temp);
        $('#carousel-dot').html(tem_p);
    };
    //动态获取公告渲染
    function renderAnnounce() {
        var a_notice=$('.avtar-notice');
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: '//av.m.jd.care/api/announce/getList',
            success: function (info) {

                if (info && info.docs && info.docs.length>=1) {
                    announceTemp(info);
                    a_notice.show();
                }else{
                    a_notice.hide();
                }
            },
            error: function (err) {
                a_notice.hide();
                // alert('服务器错误，请稍后重试！');
            }
        })
    };

    //获取公告模板
    function announceTemp(info) {
        var temp = '';
        var t_d = info.docs;
            $.each(t_d, function (i, d) {
                if (i==0) {
                    temp += '<p class="notice-left left-text  fl">' +
                        '<a href="./notice.html#'+judgeJson( d.id)+'" data-id="' +judgeJson( d._id) + '">' + judgeJson(d.title) + ' </a>' +
                        '</p>';

                }else if(i==1){
                    temp += ' <p class="notice-center hidden-sm hidden-xs  ">' +
                        '<a href="./notice.html#'+judgeJson(d.id)+'" data-id="'+judgeJson(d._id)+'">'+judgeJson(d.title)+' </a>' +
                        '</p>';

                }else{
                    return;
                }
            })

            $('#avtar-notice').html(temp);
    };
    //判断当前值，是否存在
    function judgeJson(v) {
        if (v) {
            return v;
        } else {
            return '';
        }
    };

    //判断轮播图路径
    function judgeLink(v) {
        if (v) {
            return v.replace(/(^\s*)|(\s*$)/g,'');
        } else {
            return 'javascript:;';
        }
    };

    //解决方案tab栏，不在页面可是区域时，固定定位
    function schemeClient(){
        var oprateTab=document.getElementById('oprate-tab');
        var avtarNavbar=document.getElementById('avtar-navbar');
        var avtarBannerr=document.getElementById('avtar-banner');
        if(avtarBannerr){
            var h=avtarNavbar.offsetHeight+avtarBannerr.offsetHeight;
            window.onscroll = function () {
                if (scroll().top >h) {
                    oprateTab.className = "oprate-inner fixed";
                } else {
                    oprateTab.className = "oprate-inner";
                }
            };
        }

    };


    banner();
    renderSlider(); //动态渲染轮播图图片
    renderAnnounce();   //动态获取公告渲染
    schemeClient();

})();
