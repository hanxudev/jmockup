$(function(){

    setIframeHeight();
    // viewHeader();
    // viewFooter();
});

function GetQueryString(name)
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r!=null)return  r[2]; return null;
}


// 设置iframe的高度
function setIframeHeight(){
    if( $("#rightIframe").length == 0) return;

    function setIframe(){
        $("#rightIframe").css( {
            "width" : "100%",
            "height" : parseInt( $( window ).height() ) - parseInt( $( "#breadCrumb" ).height() - parseInt( $("#wrap").height() ) )
        } );
    }


    setIframe();

    $(window).on("resize", function (  ) {
        setIframe();
    });

}

//通用顶部导航
function viewHeader(){
    var _topHeader = {};
    var _html = template('top', _topHeader);//based on the directory 'tpl';
    $('#avtar-navbar').html(_html);
}


//通用footer
function viewFooter(){
    var _footer = {};
    var _html = template('footer', _footer);//based on the directory 'tpl';
    $('.avtar-bottom').html(_html);
}