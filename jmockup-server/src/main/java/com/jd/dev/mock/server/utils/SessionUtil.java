package com.jd.dev.mock.server.utils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 获取session中的erp
 * Created by hanxu3 on 2018/8/20.
 */
public class SessionUtil {

    public final static String KEY_NAME_ERP = "erp";

    public final static String KEY_NAME_USER = "loginUser";

    public final static String KEY_NAME_USER_NUM = "user.num";

    /**
     * 获取erp
     * @param request
     * @return
     */
    public static String getErpFromSession(HttpServletRequest request){
        String erp = "";
        HttpSession session = request.getSession(false);
        if(session != null ){
            erp = (String)session.getAttribute(KEY_NAME_ERP);
        }
        return erp;
    }

    /**
     * 获取当前用户数量
     * @param request
     * @return
     */
    public static int getUserSessionNum(HttpServletRequest request){
        int num = 0;
        HttpSession session = request.getSession(false);
        if(session != null ){
            ServletContext ctx = session.getServletContext();
            if(ctx.getAttribute(KEY_NAME_USER_NUM) != null){
                num = (Integer) ctx.getAttribute(KEY_NAME_USER_NUM);
            }
        }
        return num;
    }
}
