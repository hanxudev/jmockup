package com.jd.dev.mock.server.dto;

import com.jd.fastjson.JSONObject;

import java.util.List;

/**
 * Created by zhangzhao8 on 2018/7/23.
 */
public class MethodTableResponse {
    private List data;
    private int code;
    private long count;
    private String msg;

    public MethodTableResponse(List data,long count) {
        this.data=data;
        this.count=count;
        this.code=0;
        this.msg="";
    }
    public MethodTableResponse(List data,int count,int status,String msg) {
        this.data=data;
        this.count=count;
        this.code=status;
        this.msg=msg;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data=data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code=code;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count=count;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg=msg;
    }

    public String toString(){
        JSONObject obj=new JSONObject();
        obj.put("code", this.code);
        obj.put("msg", this.msg);
        obj.put("count",this.count);
        obj.put("data",this.data);
        return obj.toString();
    }
}
