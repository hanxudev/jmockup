package com.jd.dev.mock.server.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by liukun15 on 2018/7/12.
 */
@Document(collection = "mocker.data.jsf")
public class MockJSFData extends  MockBaseData {
    @Id
    private String id;

    private String returnType;

    private String token;

    private String argsType;

    private String args;

    private String alias;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getArgsType() {
        return argsType;
    }

    public void setArgsType(String argsType) {
        this.argsType = argsType;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    @Override
    public int hashCode() {
        return getMethodName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return this.getMethodName().equals(((MockJSFData)obj).getMethodName());
    }
}
