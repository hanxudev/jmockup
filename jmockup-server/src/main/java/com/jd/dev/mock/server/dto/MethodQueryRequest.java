package com.jd.dev.mock.server.dto;

import java.util.List;

/**
 * Created by zhangzhao8 on 2018/7/21.
 */
public class MethodQueryRequest {
    //erp
    private String erp;
    //类型
    private String localClassName;
    //方法名
    private String localMethodName;
    //参数
    private List<String> args;

    public MethodQueryRequest(String erp, String localClassName, String localMethodName) {
        this.erp=erp;
        this.localClassName=localClassName;
        this.localMethodName=localMethodName;
    }
    public MethodQueryRequest(String erp, String localClassName, String localMethodName,List<String> arg) {
        this.erp=erp;
        this.localClassName=localClassName;
        this.localMethodName=localMethodName;
        this.args=arg;
    }
    public String getErp() {
        return erp;
    }

    public void setErp(String erp) {
        this.erp=erp;
    }

    public String getLocalClassName() {
        return localClassName;
    }

    public void setLocalClassName(String localClassName) {
        this.localClassName=localClassName;
    }

    public String getLocalMethodName() {
        return localMethodName;
    }

    public void setLocalMethodName(String localMethodName) {
        this.localMethodName=localMethodName;
    }

    public List<String> getArgs() {
        return args;
    }

    public void setArgs(List<String> args) {
        this.args=args;
    }
}
