package com.jd.dev.mock.server.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 项目首页
 * Created by hanxu3 on 2018/7/10.
 */
@RestController
public class IndexController {
    @RequestMapping(value = {"/index"})
    public ModelAndView index(){
        return new ModelAndView("/index");
    }

    @RequestMapping(value = {"/","/main"})
    public ModelAndView main(){
        return new ModelAndView("/main");
    }
}
