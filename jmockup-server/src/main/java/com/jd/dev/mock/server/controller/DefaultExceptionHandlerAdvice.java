package com.jd.dev.mock.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 可以自定义异常处理器
 * Created by hanxu3 on 2018/7/10.
 */
//@RestControllerAdvice
public class DefaultExceptionHandlerAdvice {
	private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandlerAdvice.class);

	@ExceptionHandler(value = {Exception.class})
	public ModelAndView exception(Exception e, HttpServletRequest request, HttpServletResponse response) {
		StringBuilder sb = new StringBuilder("ErrorInfo:");
		sb.append("RequestUrl=[").append(request.getRequestURL()).append("]");
		sb.append("QueryString=[").append(request.getQueryString()).append("]");
		logger.error(sb.toString(),e);
		ModelAndView page = new ModelAndView("error");
		page.addObject("status",response.getStatus());
		int status = response.getStatus();
		try{
			String type = HttpStatus.valueOf(status).getReasonPhrase();
			page.addObject("error",type);
		}catch (Exception e1){
			logger.error("getHttpStatus error:",e1);
		}
		page.addObject("message",e.getMessage());
		return page;
	}
}
