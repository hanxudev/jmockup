package com.jd.dev.mock.server.service;

import com.jd.dev.mock.server.jsf.GenericServiceFactory;
import com.jd.fastjson.JSON;
import com.jd.jsf.gd.GenericService;
import com.jd.jsf.gd.config.RegistryConfig;
import com.jd.jsf.gd.msg.Invocation;
import com.jd.jsf.gd.msg.RequestMessage;
import com.jd.jsf.gd.util.Constants;
import org.apache.commons.lang3.StringUtils;
import com.jd.dev.mock.common.jsf.JsfServiceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hanxu3 on 2018/5/11.
 */
@Service
public class JsfInvokeService {

    private static final Logger logger = LoggerFactory.getLogger(JsfInvokeService.class);

    @Autowired
    RegistryConfig jsfRegistry;

    /**
     * 执行jsf方法
     * @param jsfRequest
     * @return
     */
    public Object invokeJsf(JsfServiceInfo jsfRequest){
        logger.debug("JsfInvokeService.invokeJsf jsfRequest:"+ JSON.toJSONString(jsfRequest));
        GenericService service = GenericServiceFactory.getGenericService(jsfRegistry,jsfRequest.getClazzName(),jsfRequest.getAlias(),jsfRequest.getParameters());
        Object result = null;
        try {
            result = service.$invoke(jsfRequest.getMethodName(),jsfRequest.getArgsType(),jsfRequest.getArgs());
        }catch (Exception e){
            logger.error("invoke jsf service error:",e);
        }
        logger.debug("JsfInvokeService.invokeJsf result:"+ JSON.toJSONString(result));
        return result;
    }

    /**
     * 执行jsf方法-废弃
     * （RequestMessage部分属性未实现Serializable接口，hessian无法序列化）
     * @param jsfRequest
     * @return
     */
    public Object invokeJsf(RequestMessage jsfRequest){
        String token = (String)jsfRequest.getInvocationBody().getAttachment(Constants.HIDDEN_KEY_TOKEN);
        Map<String,String> parameters = null;
        if(StringUtils.isNotBlank(token)){
            parameters = new HashMap<>();
            parameters.put(Constants.HIDDEN_KEY_TOKEN,token);
        }
        GenericService service = GenericServiceFactory.getGenericService(jsfRegistry,jsfRequest.getClassName(),jsfRequest.getAlias(),parameters);

        String methodName = getMethodName(jsfRequest);
        String[] paramTypes = getParamTypes(jsfRequest);
        Object[] params = getParams(jsfRequest);
        Object result = null;
        try {
            result = service.$invoke(methodName,paramTypes,params);
        }catch (Exception e){
            logger.error("invoke jsf service error:",e);
        }
        return result;

    }

    /**
     * 获取方法名
     * @return
     */
    private String getMethodName(RequestMessage jsfRequest){
        Invocation invocation = jsfRequest.getInvocationBody();
        return invocation.getMethodName();
    }

    /**
     * 获取参数类型列表
     * @param jsfRequest
     * @return
     */
    private String[] getParamTypes(RequestMessage jsfRequest){
        Invocation invocation = jsfRequest.getInvocationBody();
        return invocation.getArgsType();
    }

    /**
     * 获取参数列表
     * @param jsfRequest
     * @return
     */
    private Object[] getParams(RequestMessage jsfRequest){
        Invocation invocation = jsfRequest.getInvocationBody();
        return invocation.getArgs();
    }

}
