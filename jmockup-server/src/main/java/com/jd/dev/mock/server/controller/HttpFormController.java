package com.jd.dev.mock.server.controller;

import com.jd.dev.mock.server.dao.mongodb.MockHttpDataRepository;
import com.jd.dev.mock.server.domain.HttpFormRequest;
import com.jd.dev.mock.server.domain.MockHttpData;
import com.jd.dev.mock.server.service.HttpClientService;
import com.jd.dev.mock.server.utils.SessionUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 处理http接口录入mock数据
 * Created by hanxu3 on 2018/7/10.
 */
@RestController
@RequestMapping("/mocker/http")
public class HttpFormController {
    private static final Logger logger = LoggerFactory.getLogger(HttpFormController.class);

    @Autowired
    HttpClientService httpClientService;

    @Autowired
    MockHttpDataRepository mockHttpDataRepository;

    @RequestMapping("/create")
    public ModelAndView createPage(){
        return new ModelAndView("views/http_form");
    }


    /**
     * 查询线上数据
     * @return
     */
    @RequestMapping("/data")
    public String queryData(HttpFormRequest httpFormRequest){
        String requestUrl = httpFormRequest.getRequestUrl();
        if(StringUtils.isBlank(requestUrl)){
            return "requestUrl must not be null";
        }
        if(!requestUrl.startsWith("http")){
            requestUrl = "http://".concat(requestUrl);
        }
        String[] requests = requestUrl.split("\\?");
        String queryString = "";
        String hostUrl = requests[0];
        if(requests.length > 0){
            queryString = requests[1];
        }
        StringBuilder requestBuilder = new StringBuilder(hostUrl);
        if(StringUtils.isNotBlank(queryString)){
            requestBuilder.append("?");
            String[] params = queryString.split("&");
            for(String param : params){
                try{
                    String[] temp = param.split("=");
                    requestBuilder.append(temp[0]).append("=").append(URLEncoder.encode(temp[1],"utf-8")).append("&");
                }catch (Exception e){
                    logger.error("URLEncoder Param error:",e);
                }
            }
        }
        //去除最后的“&”
        requestBuilder.deleteCharAt(requestBuilder.length() -1);
        String response = httpClientService.sendGet(requestBuilder.toString(),null,null,null);
        return response;
    }

    /**
     * mock数据
     * @param httpFormRequest
     * @return
     */
    @RequestMapping("/save")
    public MockHttpData saveMockData(HttpFormRequest httpFormRequest, HttpServletRequest request){
        MockHttpData httpData = new MockHttpData();
        httpData.setErp(SessionUtil.getErpFromSession(request));
        httpData.setResultData(httpFormRequest.getMockData());
        httpData.setType("http");
        httpData.setMethodName(httpFormRequest.getHostUrl());
        httpData.setMethodDesc(httpFormRequest.getDesc());
        httpData.setRequestUrl(httpFormRequest.getRequestUrl());
        httpData.setHeaders(httpFormRequest.getHeaders());
        httpData.setPostData(httpFormRequest.getPostData());
        httpData.setStatus(1);
        return mockHttpDataRepository.save(httpData);
    }

    /**
     * 线上已有数据拉取的表格数据
     * @param page
     * @param limit
     * @param mockHttpData
     * @return
     */
    @RequestMapping("/table")
    public Map<String,Object> getMockDataTable(String page,String limit,MockHttpData mockHttpData){
        Map<String,Object> result = new HashMap<>(4);

        int pageNum = StringUtils.isBlank(page) ? 0 : Integer.parseInt(page) - 1;
        int pageSize =StringUtils.isBlank(limit) ? 10 : Integer.parseInt(limit);
        Pageable pageable = new PageRequest(pageNum,pageSize);
        if(StringUtils.isBlank(mockHttpData.getErp())){
            //erp为空设置NULL值使mongo查询取消匹配
            mockHttpData.setErp(null);
        }

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("methodName", ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("methodDesc", ExampleMatcher.GenericPropertyMatchers.contains())
                .withIgnorePaths("modifyDate")
                .withIgnorePaths("resultData")
                .withIgnorePaths("status")
                .withIgnoreCase(true)//忽略大小写
                .withIgnoreNullValues();
        Example<MockHttpData> example = Example.of(mockHttpData,matcher);
        Page pageData = mockHttpDataRepository.findAll(example,pageable);
        List<MockHttpData> datas = pageData.getContent();
        result.put("code",0);
        result.put("data",datas);
        result.put("count",pageData.getTotalElements());
        return result;
    }

    /**
     * 模糊匹配线上接口，自动填充表单
     * @param search
     * @return
     */
    @RequestMapping("/autoComplete")
    public Map<String,Object> getAutoCompleteData(String search){
        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withMatcher("methodName", ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("methodDesc", ExampleMatcher.GenericPropertyMatchers.contains())
                .withIgnoreCase(true)//忽略大小写
                .withIgnorePaths("status")
                .withIgnoreNullValues();
        MockHttpData exampleData = new MockHttpData();
        exampleData.setMethodName(search);
        exampleData.setMethodDesc(search);
        Example<MockHttpData> example = Example.of(exampleData,matcher);
        List<MockHttpData> datas = mockHttpDataRepository.findAll(example);
        Map<String,Object> result = new HashMap<>();
        result.put("data",datas);
        return result;
    }
}
