package com.jd.dev.mock.server.dao.mongodb;

import com.jd.dev.mock.server.domain.MockMethodData;
import org.springframework.data.mongodb.repository.DeleteQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by zhangzhao8 on 2018/7/14.
 */
@Repository
public interface MockMethodRepository  extends MongoRepository<MockMethodData,Long>,CustomMockMethodRepository {
    @Query(value = "{'_id':?0}")
    public MockMethodData findById(String id);

    @DeleteQuery(value = "{'_id':?0}")
    public Long deleteById(String id);
}
