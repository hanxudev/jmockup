package com.jd.dev.mock.server.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by liukun15 on 2018/7/30.
 */
@Document(collection = "mocker.data.application")
public class ApplicationData {
    String ip;
    Long id;
    String token;
    String appName;
    private String appNameCn;
    String erp;
    int types;
    int status;
    private Date createDate = new Date();

    public ApplicationData(Long id, String token, String appName, String appNameCn) {
        this.id = id;
        this.token = token;
        this.appName = appName;
        this.appNameCn = appNameCn;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public ApplicationData() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppNameCn() {
        return appNameCn;
    }

    public void setAppNameCn(String appNameCn) {
        this.appNameCn = appNameCn;
    }

    public String getErp() {
        return erp;
    }

    public void setErp(String erp) {
        this.erp = erp;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getTypes() {
        return types;
    }

    public void setTypes(int types) {
        this.types = types;
    }

    public enum  Types{
        DEV(1),TEST(2),OTHER(3),;
        int typeValue;
        Types(int typeValue){
            this.typeValue = typeValue;
        }

        public int getTypeValue() {
            return typeValue;
        }
    }

    @Override
    public boolean equals(Object obj) {
        return this.id.equals(((ApplicationData)obj).getId());
    }
}
