package com.jd.dev.mock.server.config;

import com.jd.dev.mock.server.interceptor.HttpMockFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * httpMock代理
 */
@Configuration
public class WebFilterConfig {
    @Bean
    public FilterRegistrationBean greetingFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setName("httpMock");
        HttpMockFilter mockFilter = new HttpMockFilter();
        registrationBean.setFilter(mockFilter);
        registrationBean.setOrder(1);
        List<String> urlList = new ArrayList<String>();
        urlList.add("/*");
        registrationBean.setUrlPatterns(urlList);
        return registrationBean;
    }
}