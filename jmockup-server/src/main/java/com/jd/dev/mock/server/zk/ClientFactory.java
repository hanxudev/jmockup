package com.jd.dev.mock.server.zk;

import org.apache.curator.framework.AuthInfo;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.server.auth.DigestAuthenticationProvider;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class ClientFactory {

	private static  CuratorFramework client ;

	private static  CuratorFramework betaClient;
	
	public static CuratorFramework getTestInstance() {
		
		if (client != null) {
			if (client.getState().compareTo(CuratorFrameworkState.STARTED) !=0) {
				client.start();
			}
			return client;
		} else {
			client = newClient("192.168.192.48:2181");
			client.start();
			return client;
		}
	}

	/**
	 * 预发布配置
	 * @return
	 */
	public static CuratorFramework getBetaInstance(String connectionString) {

		if (betaClient != null) {
			if (betaClient.getState().compareTo(CuratorFrameworkState.STARTED) !=0) {
				betaClient.start();
			}
			return betaClient;
		} else {
			betaClient = newClient(connectionString);
			betaClient.start();
			return betaClient;
		}
	}

	public static CuratorFramework newClient(String connectionString) {
		CuratorFramework client = CuratorFrameworkFactory.builder().connectString(connectionString)
				.sessionTimeoutMs(10000)
				.connectionTimeoutMs(3000)
				.canBeReadOnly(false)
				.retryPolicy(new ExponentialBackoffRetry(1000, 1))
				.build();
		return client;
	}
	
	public static CuratorFramework newClientAcl() {
		String up = "";
		try {
			 up = DigestAuthenticationProvider.generateDigest("admin:admin123321");
			 System.out.println(up);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		
		AuthInfo authinfo = new AuthInfo("digest", "admin:admin123321".getBytes());
		List<AuthInfo> aList = new ArrayList<AuthInfo>();
		aList.add(authinfo);
		
		// -Dzookeeper.DigestAuthenticationProvider.superDigest=admin:XNayzH0KHS9YkzwRXa0HNAvPPMc=
		CuratorFramework client = CuratorFrameworkFactory.builder().connectString("192.168.1.5:2181")
				.sessionTimeoutMs(5000)
				.connectionTimeoutMs(30000)
				.canBeReadOnly(false)
				.retryPolicy(new ExponentialBackoffRetry(1000, Integer.MAX_VALUE))
				.namespace("Test")
				.defaultData(null)
				.authorization("digest", "admin:admin123321".getBytes())
				//.authorization(aList)
				.build();
		return client;
		
	}
	
	
	public static void main(String[] args) {
		try {
			String digest = DigestAuthenticationProvider.generateDigest("admin:admin123321");
			System.out.println(digest);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
}
