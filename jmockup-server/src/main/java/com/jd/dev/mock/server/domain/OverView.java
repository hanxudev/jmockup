package com.jd.dev.mock.server.domain;

import java.util.List;

/**
 * 概览页数据
 * Created by hanxu3 on 2018/8/31.
 */
public class OverView {
    /**
     * 接入应用总数
     */
    private long appTotalNum;
    /**
     * 新接入应用数
     */
    private long appNewNum;

    /**
     * 在线用户数
     */
    private long userNum;

    /**
     * 当天活跃用户数
     */
    private long userDayNum;

    /**
     * 请求量
     */
    private long requestNum;

    /**
     * jsf请求量
     */
    private long jsfReqNum;

    /**
     * 用户访问量
     */
    private long pvNum;

    /**
     * 接口数
     */
    private long interfaceNum;

    /**
     * 新增接口数
     */
    private long interfaceNewNum;

    private List<UserBehavior> userList;


    public long getAppTotalNum() {
        return appTotalNum;
    }

    public void setAppTotalNum(long appTotalNum) {
        this.appTotalNum = appTotalNum;
    }

    public long getAppNewNum() {
        return appNewNum;
    }

    public void setAppNewNum(long appNewNum) {
        this.appNewNum = appNewNum;
    }

    public long getUserNum() {
        return userNum;
    }

    public void setUserNum(long userNum) {
        this.userNum = userNum;
    }

    public long getRequestNum() {
        return requestNum;
    }

    public void setRequestNum(long requestNum) {
        this.requestNum = requestNum;
    }

    public long getJsfReqNum() {
        return jsfReqNum;
    }

    public void setJsfReqNum(long jsfReqNum) {
        this.jsfReqNum = jsfReqNum;
    }

    public long getPvNum() {
        return pvNum;
    }

    public void setPvNum(long pvNum) {
        this.pvNum = pvNum;
    }

    public long getInterfaceNum() {
        return interfaceNum;
    }

    public void setInterfaceNum(long interfaceNum) {
        this.interfaceNum = interfaceNum;
    }

    public long getInterfaceNewNum() {
        return interfaceNewNum;
    }

    public void setInterfaceNewNum(long interfaceNewNum) {
        this.interfaceNewNum = interfaceNewNum;
    }

    public long getUserDayNum() {
        return userDayNum;
    }

    public void setUserDayNum(long userDayNum) {
        this.userDayNum = userDayNum;
    }

    public List<UserBehavior> getUserList() {
        return userList;
    }

    public void setUserList(List<UserBehavior> userList) {
        this.userList = userList;
    }
}
