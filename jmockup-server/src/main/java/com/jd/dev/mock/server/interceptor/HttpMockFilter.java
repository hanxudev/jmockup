package com.jd.dev.mock.server.interceptor;

import com.jd.dev.mock.server.service.AppTokenService;
import com.jd.dev.mock.server.service.HttpDataService;
import com.jd.dev.mock.server.domain.MockHttpData;
import com.jd.dev.mock.server.service.HttpProxyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 处理httpMock请求
 */
public class HttpMockFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(HttpMockFilter.class);

    private HttpDataService httpDataService = null;
    private HttpProxyService httpProxyService = null;
    private AppTokenService appTokenService = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext context = filterConfig.getServletContext();
        ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(context);
        appTokenService = (AppTokenService)ac.getBean("appTokenService");
        httpDataService = (HttpDataService)ac.getBean("httpDataService");
        httpProxyService = (HttpProxyService)ac.getBean("httpProxyService");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        logRequest(servletRequest);
        if(servletRequest instanceof HttpServletRequest){
            try{
                HttpServletRequest request = (HttpServletRequest) servletRequest;
                //存在mocker header的请求要求走mocker逻辑
                String mocker = request.getHeader("mocker");
                if("1".equals(mocker)){
                    String res = null;
                    //查询mock数据
                    String token = request.getHeader("token");
                    String erp = appTokenService.getErpByToken(token);
                    String methodName = request.getRequestURL().toString();
                    MockHttpData mockHttpData = httpDataService.queryHttpMockData(methodName,erp);
                    if(mockHttpData != null){
                        res = mockHttpData.getResultData();
                        writeResponse(res,servletResponse,"utf-8");
                        return;
                    }
                    //代理请求
                    res = httpProxyService.invokeHttpRequest(request);
                    writeResponse(res,servletResponse,"utf-8");
                    return;
                }
            }catch (Exception e){
                logger.error("HttpMockFilter.doFilter error:",e);
            }
        }
        filterChain.doFilter(servletRequest,servletResponse);
    }
    @Override
    public void destroy() {
    }

    /**
     * 写返回数据
     * @param res
     * @param response
     */
    private void writeResponse(String res, ServletResponse response, String encoding) {
        PrintWriter writer = null;
        try {
            response.setCharacterEncoding(encoding);
            writer = response.getWriter();
            if (res != null) {
                writer.write(res);
            } else {
                writer.write("");
            }
            writer.flush();
        } catch (Exception e) {
            logger.error("writeToClient error", e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (Exception e2) {
                }
            }
        }
    }

    /**
     * 记录日志
     * @param servletRequest
     */
    private void logRequest(ServletRequest servletRequest){
        if(logger.isDebugEnabled()){
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            logger.info("请求地址："+request.getRequestURL());
        }
    }
}