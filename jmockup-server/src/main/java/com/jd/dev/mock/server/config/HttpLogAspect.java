package com.jd.dev.mock.server.config;

import com.alibaba.fastjson.JSON;
import com.jd.dev.mock.server.domain.log.HttpLogData;
import com.jd.dev.mock.server.service.async.RemoteLogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 拦截记录Http请求数据
 */
@Aspect
@Component
public class HttpLogAspect {

    private static final Logger logger = LoggerFactory.getLogger(HttpLogAspect.class);

    private static final ThreadLocal<HttpLogData> tempTreadLocal = new ThreadLocal<>();

    @Autowired
    RemoteLogService remoteLogService;

    @Pointcut("execution(* com.jd.dev.mock.server.service.HttpClientService.*(..))")
    public void requestLog() {
    }

    @Before("requestLog()")
    public void before(JoinPoint joinPoint) {
        try{
            long startTime = System.currentTimeMillis();
            HttpLogData httpLogData = new HttpLogData();
            httpLogData.setRequestTime(startTime);
            joinPoint.getSignature().getName();
            //解析参数
            Object[] args = joinPoint.getArgs();
            if(args != null && args.length > 0){
                String requestUrl = (String)args[0];
                String params = JSON.toJSONString(args[1]);
                String headers = JSON.toJSONString(args[2]);
                String cookies = JSON.toJSONString(args[3]);
                httpLogData.setMethodKey(requestUrl);
                String[] temp = requestUrl.split("\\?");
                httpLogData.setMethodName(temp[0]);
                if(temp.length > 1){
                    httpLogData.setReqest(temp[1]);
                }
            }
            tempTreadLocal.set(httpLogData);
        }catch (Exception e){
            logger.error("beforeJoinPoint error:",e);
        }
    }

    @After("requestLog()")
    public void after() {
    }

    @AfterReturning(returning = "result", pointcut = "requestLog()")
    public Object afterReturn(Object result) {
        try{
            HttpLogData httpLogData = tempTreadLocal.get();
            httpLogData.setReturnTime(System.currentTimeMillis());
            httpLogData.setResult(JSON.toJSONString(result));
            remoteLogService.logHttpRequest(httpLogData);
        }catch (Exception e){
            logger.error("JsfLogAspect afterReturn error:",e);
        }
        return result;
    }
}