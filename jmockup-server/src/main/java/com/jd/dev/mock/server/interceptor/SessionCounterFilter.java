package com.jd.dev.mock.server.interceptor;

import com.jd.dev.mock.server.service.RedisService;
import com.jd.dev.mock.server.utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * 统计在线人数
 */
@WebListener
public class SessionCounterFilter implements HttpSessionListener {

    @Autowired
    RedisService redisService;

    /**
     * 后续改写到redis里
     */
    private static volatile int activeSessions = 0;

    /**
     *  Session创建事件
     **/
    @Override
    public void sessionCreated(HttpSessionEvent event) {
        int asn = getActiveSessions(event);
        if(asn > activeSessions){
            activeSessions = asn;
        }
        activeSessions ++;
        ServletContext ctx = event.getSession().getServletContext();
        ctx.setAttribute(SessionUtil.KEY_NAME_USER_NUM, activeSessions);
    }

    /**
     *  Session失效事件
     *
     **/
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        int asn = getActiveSessions(se);
        if(asn > activeSessions){
            activeSessions = asn;
        }
        activeSessions --;
        ServletContext ctx = se.getSession().getServletContext();
        ctx.setAttribute(SessionUtil.KEY_NAME_USER_NUM, activeSessions);
    }

    /**
     * 获取session数量
     * @param event
     * @return
     */
    private int getActiveSessions(HttpSessionEvent event){
        ServletContext ctx = event.getSession().getServletContext();
        if(ctx != null){
            Object attr = ctx.getAttribute(SessionUtil.KEY_NAME_USER_NUM);
            if(attr != null){
                return (Integer)attr;
            }
        }
        return 0;
    }
} 