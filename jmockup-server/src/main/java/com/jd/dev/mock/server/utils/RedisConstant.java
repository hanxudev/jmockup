package com.jd.dev.mock.server.utils;

/**
 * Created by hanxu3 on 2018/8/31.
 */
public class RedisConstant {
    public final static String KEY_REQ_COUNT = "key.req.count";

    public final static String KEY_LOGIN_USER = "key.login.user";

    /**
     * 用户状态记录多少个
     */
    public final static int KEY_USER_LOG_NUM = 6;

    public final static String KEY_PV_CPUNT_SET = "pv.count.set";

    /**
     * 计算天的秒值的时候除的值
     */
    public final static long TIME_DAY_LONG = 1000 * 60 * 60 * 24;
}
