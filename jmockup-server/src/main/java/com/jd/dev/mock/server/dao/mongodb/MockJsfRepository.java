package com.jd.dev.mock.server.dao.mongodb;

import com.jd.dev.mock.server.domain.MockJSFData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 根据erp和方法名查询mock数据
 * Created by duanliping3 on 2018/7/23.
 */
@Repository
public interface MockJsfRepository extends MongoRepository<MockJSFData,Long> {
    MockJSFData findFirstByErpAndMethodName(String erp, String methodName);
}
