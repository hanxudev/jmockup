package com.jd.dev.mock.server.dto;

/**
 * Created by zhangzhao8 on 2018/7/21.
 */
public class MethodFormRequest {
    private String id;
    //类型
    private String localClassName;
    //方法名
    private String localMethodName;
    //参数类名，多个以,分隔
    private String argsClassName;
    //返回实现类名
    private String returnType;

    private String methodDesc;

    private String resultData;

    private String erp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id=id;
    }

    public String getLocalClassName() {
        return localClassName;
    }

    public void setLocalClassName(String localClassName) {
        this.localClassName=localClassName;
    }

    public String getLocalMethodName() {
        return localMethodName;
    }

    public void setLocalMethodName(String localMethodName) {
        this.localMethodName=localMethodName;
    }

    public String getArgsClassName() {
        return argsClassName;
    }

    public void setArgsClassName(String argsClassName) {
        this.argsClassName=argsClassName;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType=returnType;
    }

    public String getMethodDesc() {
        return methodDesc;
    }

    public void setMethodDesc(String methodDesc) {
        this.methodDesc=methodDesc;
    }

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData=resultData;
    }

    public String getErp() {
        return erp;
    }

    public void setErp(String erp) {
        this.erp = erp;
    }
}
