package com.jd.dev.mock.server.config;

import com.alibaba.fastjson.JSON;
import com.jd.dev.mock.common.jsf.JsfServiceInfo;
import com.jd.dev.mock.server.service.async.RemoteLogService;
import com.jd.dev.mock.server.domain.log.JsfLogData;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 拦截记录Jsf请求数据
 */
@Aspect
@Component
public class JsfLogAspect {

    private static final Logger logger = LoggerFactory.getLogger(JsfLogAspect.class);

    private static final ThreadLocal<JsfLogData> tempTreadLocal = new ThreadLocal<>();

    @Autowired
    RemoteLogService remoteLogService;

    @Pointcut("execution(* com.jd.dev.mock.server.service.JsfInvokeService.*(..))")
    public void requestLog() {
    }

    @Before("requestLog()")
    public void before(JoinPoint joinPoint) {
        try{
            long startTime = System.currentTimeMillis();
            JsfLogData jsfLogData = new JsfLogData();
            jsfLogData.setRequestTime(startTime);
            //解析参数
            Object[] args = joinPoint.getArgs();
            if(args != null && args.length > 0){
                for(Object arg : args){
                    if(arg instanceof JsfServiceInfo){
                        JsfServiceInfo info = (JsfServiceInfo)arg;
                        jsfLogData.setJsfServiceInfo(JSON.toJSONString(info));
                        String methodName = info.getClazzName().concat(".").concat(info.getMethodName());
                        jsfLogData.setMethodName(methodName);
                        jsfLogData.setMethodKey(methodName.concat("~").concat(((JsfServiceInfo) arg).getAlias()));
                        jsfLogData.setReqest(JSON.toJSONString(info.getArgs()));
                    }
                }
            }
            tempTreadLocal.set(jsfLogData);
        }catch (Exception e){
            logger.error("beforeJoinPoint error:",e);
        }
    }

    @After("requestLog()")
    public void after() {
    }

    @AfterReturning(returning = "result", pointcut = "requestLog()")
    public Object afterReturn(Object result) {
        try{
            JsfLogData jsfLogData = tempTreadLocal.get();
            jsfLogData.setReturnTime(System.currentTimeMillis());
            jsfLogData.setResult(JSON.toJSONString(result));
            remoteLogService.logJsfRequest(jsfLogData);
        }catch (Exception e){
            logger.error("JsfLogAspect afterReturn error:",e);
        }
        return result;
    }
}