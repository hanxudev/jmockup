package com.jd.dev.mock.server.dao.mongodb;

import com.jd.dev.mock.server.domain.MockHttpData;
import com.jd.dev.mock.server.domain.MockJSFData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by liukun15 on 2018/7/12.
 */
@Repository
public interface MockJSFDataRepository extends MongoRepository<MockJSFData,String> {
    @Query(value="{'id':?0}")
    MockJSFData findMockJSFDataById(String id);

    @Query(value="{'methodName':?0}")
    Page<MockHttpData> findMockJSFDataByMethodName(String methodName, Pageable pageable);

    @Query(value="{'type':?0}")
    Page<MockJSFData> findByTypeLike(String type, Pageable pageable);

    @Query(value="{'erp':?0}")
    Page<MockJSFData> findByErpLike(String erp, Pageable pageable);

    @Query(value="{'type':?0,erp:?1}")
    Page<MockJSFData> findByTypeAndErpLike(String type, String erp, Pageable pageable);
}
