package com.jd.dev.mock.server.domain.log;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by hanxu3 on 2018/7/19.
 */
@Document(collection = "mocker.log.http")
public class HttpLogData extends BaseLogData {
    //json格式的参数
    private String params;
    //json格式的headers
    private String headers;
    //json格式的cookie
    private String cookies;
    private long requestTime;
    private long returnTime;

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public long getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(long returnTime) {
        this.returnTime = returnTime;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getCookies() {
        return cookies;
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }
}
