package com.jd.dev.mock.server.domain;

import org.apache.commons.lang3.time.DateUtils;

/**
 * 用户动态
 * Created by hanxu3 on 2018/9/3.
 */
public class UserBehavior {

    /**
     * 用户名称
     */
    private String name;

    /**
     * 时间
     */
    private long time;

    /**
     * String类型时间点
     */
    private String timeDate;

    /**
     * 行为
     */
    private String behavior;
    /**
     * 展示的时间戳（几秒前）
     */
    private String timeTag;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior;
    }

    public String getTimeTag() {
        long cur = System.currentTimeMillis();
        long temp = cur - time;//时间差X秒
        if(temp < DateUtils.MILLIS_PER_MINUTE){
            timeTag =  (temp/DateUtils.MILLIS_PER_SECOND ) + "秒前";
        }else if (temp < DateUtils.MILLIS_PER_HOUR){
            timeTag = (temp/DateUtils.MILLIS_PER_MINUTE) + "分钟前";
        }else {
            timeTag = (temp/DateUtils.MILLIS_PER_HOUR) + "小时前";
        }
        return timeTag;
    }

    public void setTimeTag(String timeTag) {
        this.timeTag = timeTag;
    }

    public String getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(String timeDate) {
        this.timeDate = timeDate;
    }
}
