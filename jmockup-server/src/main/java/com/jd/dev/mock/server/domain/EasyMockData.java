package com.jd.dev.mock.server.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by liukun15 on 2018/8/8.
 */
@Document(collection = "mocker.data.easymock")
public class EasyMockData {
    @Id
    String id;
    String easyMockDesc;
    String url;
    String createTime;
    String erp;
    String resultData;

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }

    public String getMockId() {
        return id;
    }

    public void setMockId(String mockId) {
        this.id = mockId;
    }

    public String getEasyMockDesc() {
        return easyMockDesc;
    }

    public void setEasyMockDesc(String easyMockDesc) {
        this.easyMockDesc = easyMockDesc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getErp() {
        return erp;
    }

    public void setErp(String erp) {
        this.erp = erp;
    }
}
