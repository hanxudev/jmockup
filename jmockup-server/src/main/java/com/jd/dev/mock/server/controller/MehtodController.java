package com.jd.dev.mock.server.controller;

import com.alibaba.fastjson.JSON;
import com.jd.dev.mock.server.domain.MockMethodData;
import com.jd.dev.mock.server.dto.MethodTableResponse;
import com.jd.dev.mock.server.dto.MethodFormRequest;
import com.jd.dev.mock.server.dto.MethodQueryRequest;
import com.jd.dev.mock.server.service.MockMethodService;
import com.jd.dev.mock.server.utils.SessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangzhao8 on 2018/7/21.
 */
@RestController
@RequestMapping("/method")
public class MehtodController {
    private static final Logger logger = LoggerFactory.getLogger(MehtodController.class);

    @Autowired
    private MockMethodService mockMethodService;

    @ResponseBody
    @RequestMapping(value="/data/save",method =RequestMethod.POST )
    public Map save(@RequestBody  MethodFormRequest formData,HttpServletRequest request){
        formData.setErp(SessionUtil.getErpFromSession(request));
        if(StringUtils.isEmpty(formData.getId())){
            mockMethodService.save(formData);
        }else{
            mockMethodService.save(formData,formData.getId());
        }
        Map map=new HashMap<>();
        map.put("code",0);
        map.put("msg", "success");
        return map;
    }

    @ResponseBody
    @RequestMapping(value="/data/copy",method =RequestMethod.POST )
    public Map copy(@RequestParam("id") String id){
        mockMethodService.copy(id);
        Map map=new HashMap<>();
        map.put("code",0);
        map.put("msg","success");
        return map;
    }

    @ResponseBody
    @RequestMapping(value="/data/list",method =RequestMethod.GET )
    public String list(HttpServletRequest request){

        String erp=request.getParameter("erp");
        String className=request.getParameter("className");
        String methodName=request.getParameter("methodName");
        int page=Integer.parseInt(request.getParameter("page"));
        int limit=Integer.parseInt(request.getParameter("limit"));
        MethodQueryRequest methodQueryRequest=new MethodQueryRequest(erp,className,methodName);
        if(limit==0){
            limit=10;
        }
        Map map=mockMethodService.findAll(methodQueryRequest,page,limit);
        MethodTableResponse response=new MethodTableResponse((List)map.get("data"),(Long)map.get("count"));
        logger.debug("response:"+JSON.toJSONString(response));
        return response.toString();
    }

    @ResponseBody
    @RequestMapping(value="/data/findOne",method =RequestMethod.GET )
    public MockMethodData getData(@RequestParam("id") String id){
        logger.debug("id:"+id);
        return  mockMethodService.getById(id);
    }

    @ResponseBody
    @RequestMapping(value="/data/del",method =RequestMethod.POST )
    public Map del(@RequestParam("id") String id){
        mockMethodService.delete(id);
        Map map=new HashMap<>();
        map.put("code",0);
        map.put("msg", "success");
        return map;
    }
}
