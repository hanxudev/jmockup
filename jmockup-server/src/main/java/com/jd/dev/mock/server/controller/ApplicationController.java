package com.jd.dev.mock.server.controller;

import com.alibaba.fastjson.JSON;
import com.jd.dev.mock.server.domain.ApplicationData;
import com.jd.jone.api.v2.beans.sysapp.AppWithSysInfo;
import com.jd.jone.api.v2.exception.JoneException;
import com.jd.jone.api.v2.service.JoneAppService;
import com.jd.dev.mock.server.dao.mongodb.ApplicationDataRepository;
import com.jd.dev.mock.server.service.RedisService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * Created by liukun15 on 2018/7/30.
 */
@RestController
@RequestMapping("/mocker")
public class ApplicationController {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
    private static final String APPLICATION_KEY = "app_info_";

    @Autowired
    ApplicationDataRepository applicationDataRepository;
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    JoneAppService joneAppService;
    @Autowired
    RedisService redisService;

    @RequestMapping("/applicationPage")
    public ModelAndView applicationPage() {
        return new ModelAndView("views/application_rec");
    }

    /**
     * 获取erp对应的应用信息
     * @return
     */
    @RequestMapping("/getApplicationInfo")
    public Map<String, Object> getApplicationInfo(String erp, int page, int limit) throws JoneException {
        if (StringUtils.isEmpty(erp)) {
            logger.error("erp不可为空");
            return null;
        }
        Map<String, Object> result = new HashMap<>();
        List<AppWithSysInfo> appWithSysInfos = redisService.getAppInfoByErp(APPLICATION_KEY + erp);
        if( null == appWithSysInfos || appWithSysInfos.size() == 0){
            appWithSysInfos = joneAppService.queryAppByErp(erp);
            redisService.putAppInfo(APPLICATION_KEY + erp,appWithSysInfos);
        }
        List<ApplicationData> applicationDataList = applicationDataRepository.findApplicationDataByErp(erp);
        if(!appWithSysInfos.isEmpty()){
            List<ApplicationData> applicationDatas = new ArrayList<>();
            for (AppWithSysInfo appInfo : appWithSysInfos) {
                ApplicationData applicationData = new ApplicationData();
                applicationData.setErp(erp);
                BeanUtils.copyProperties(appInfo, applicationData);
                if(applicationDataList.size() != 0 && applicationDataList.contains(applicationData)){
                    applicationData.setStatus(1);
                    applicationData.setToken(applicationDataList.get(applicationDataList.indexOf(applicationData)).getToken());
                }
                applicationDatas.add(applicationData);
            }
            if (!applicationDatas.isEmpty()) {
                logger.info("getApplicationInfo执行参数erp:{}，执行的结果是{}", JSON.toJSONString(erp), JSON.toJSONString(appWithSysInfos));
                result.put("data", getSubListByPage(applicationDatas,page,limit));
                result.put("code", 0);
                result.put("count", applicationDatas.size());
            }
        }

        return result;
    }

    /**
     * 分页
     * @param applicationDatas
     * @param page
     * @param limit
     * @return
     */
    private Object getSubListByPage(List<ApplicationData> applicationDatas, int page, int limit) {
        int start = (page-1)*limit;
        int end = page * limit;
        int size = applicationDatas.size();
        end = end >= size ? size : end;
        start = start <= 0 ? 0 : start;
        return applicationDatas.subList(start,end);
    }


    /**
     * 保存表单
     * @return
     */
    @RequestMapping("/saveApplication")
    public Map<String, Object> saveApplication(ApplicationData applicationData) {
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnoreNullValues();
        Example<ApplicationData> example = Example.of(applicationData,matcher);
        List<ApplicationData> applicationDataList = applicationDataRepository.findAll(example);
        Map<String, Object> result = new HashMap<>();
        if (applicationDataList.size() == 0) {
            try {
                String token = UUID.randomUUID().toString().replaceAll("-", "").concat(System.currentTimeMillis() + "");
                applicationData.setToken(token);
                applicationData.setStatus(1);
                ApplicationData callback = applicationDataRepository.save(applicationData);
                if (callback != null) {
                    result.put("code", 0);
                    result.put("result", "success");
                    result.put("token", callback.getToken());
                }
            } catch (Exception e) {
                result.put("code", -1);
                result.put("errorMsg", e.getMessage());
            }
        }else{
            logger.error("该应用已经生成token");
            return null;
        }

        return result;
    }


    /**
     * 跳转编辑页
     * @param id
     * @param erp
     * @param appName
     * @param appNameCn
     * @return
     */
    @RequestMapping("/editApplication")
    public ModelAndView editApplication(String id, String erp, String appName, String appNameCn,String token) {
        ModelAndView mv = new ModelAndView();
        token = token.equals("") ? null:token;
        mv.addObject("id", id);
        mv.addObject("erp", erp);
        mv.addObject("appName", appName);
        mv.addObject("appNameCn", appNameCn);
        mv.addObject("token",token);
        mv.setViewName("views/application_edit");
        return mv;
    }
}
