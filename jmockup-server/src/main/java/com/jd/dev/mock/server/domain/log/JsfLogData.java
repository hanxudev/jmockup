package com.jd.dev.mock.server.domain.log;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by hanxu3 on 2018/7/19.
 */
@Document(collection = "mocker.log.jsf")
public class JsfLogData extends BaseLogData {
    private String jsfServiceInfo;
    private long requestTime;
    private long returnTime;

    public String getJsfServiceInfo() {
        return jsfServiceInfo;
    }

    public void setJsfServiceInfo(String jsfServiceInfo) {
        this.jsfServiceInfo = jsfServiceInfo;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public long getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(long returnTime) {
        this.returnTime = returnTime;
    }
}
