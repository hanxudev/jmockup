package com.jd.dev.mock.server.service;

import com.jd.dev.mock.server.dao.mongodb.MockHttpDataRepository;
import com.jd.dev.mock.server.domain.MockHttpData;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Created by hanxu3 on 2018/8/2.
 */
@Service
public class HttpProxyService {

    private static final Logger logger = LoggerFactory.getLogger(HttpProxyService.class);

    @Autowired
    HttpClientService httpClientService;

    @Autowired
    MockHttpDataRepository mockHttpDataRepository;

    /**
     * 返回string内容
     * @param request
     * @return
     */
    public String invokeHttpRequest(HttpServletRequest request){

        //参数
        Map<String,String> paramsMap = new HashMap<>();
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()){
            String paramName = params.nextElement();
            paramsMap.put(paramName,request.getParameter(paramName));
        }

        //headers
        String erp = "";
        Map<String,String> headersMap = new HashMap<>();
        Enumeration<String> headers = request.getHeaderNames();
        while (headers.hasMoreElements()){
            String headerName = headers.nextElement();
            headersMap.put(headerName,request.getHeader(headerName));
            if("erp".equals(headerName)){
                erp = request.getHeader(headerName);
            }
        }

        //cookies
        List<org.apache.http.cookie.Cookie> clientCookies = new ArrayList<>();
        Cookie[] cookies = request.getCookies();

        String host = "";
        String url = request.getRequestURL().toString();
        try {
            URL reqUrl = new URL(url);
            host = reqUrl.getHost();
        } catch (MalformedURLException e) {
            logger.error("reqUrl error :",e);
        }


        if(cookies != null && cookies.length > 0){
            for(Cookie cookie : cookies){
                BasicClientCookie c = new BasicClientCookie(cookie.getName(),cookie.getValue());
                logger.error("cookie path:"+cookie.getDomain());
                logger.error("cookie path:"+cookie.getPath());
                c.setDomain(host);
//                c.setPath(cookie.getPath());
                clientCookies.add(c);
            }
        }

        String method = request.getMethod();
        String res = "";

        //查询mock数据是否存在
        MockHttpData mockHttpData = queryHttpMock(url,erp);
        if(mockHttpData != null && StringUtils.isNotBlank(mockHttpData.getResultData())){
            return mockHttpData.getResultData();
        }

        if(HttpMethod.GET.matches(method)){
            //处理Get请求
            String queryString = request.getQueryString();
            if(StringUtils.isNotBlank(queryString)){
                url = url.concat("?").concat(queryString);
            }
            res = httpClientService.sendGetwithCookies(url,paramsMap,headersMap,clientCookies);
        }else if(HttpMethod.POST.matches(method)){
            //post处理
            res = httpClientService.postRequest(url,paramsMap,headersMap,clientCookies);
        }
        return res;
    }

    /**
     * 查询mock数据
     */
    private MockHttpData queryHttpMock(String url,String erp){
        MockHttpData data = null;
        try{
            ExampleMatcher matcher = ExampleMatcher.matching()
                    .withIgnoreCase(true)//忽略大小写
                    .withIgnorePaths("type")
                    .withIgnorePaths("modifyDate")
                    .withIgnorePaths("methodDesc")
                    .withIgnorePaths("resultData")
                    .withIgnorePaths("ip")
                    .withIgnorePaths("pin")
                    .withIgnoreNullValues();
            MockHttpData exampleData = new MockHttpData();
            exampleData.setMethodName(url);
            exampleData.setErp(erp);
            exampleData.setStatus(1);

            Example<MockHttpData> example = Example.of(exampleData,matcher);

            List<MockHttpData> datas = mockHttpDataRepository.findAll(example);
            if(CollectionUtils.isNotEmpty(datas)){
                data = datas.get(0);
            }
        }catch (Exception e){
            logger.error("find http mock data error:",e);
        }
        return data;
    }
}
