package com.jd.dev.mock.server.config;

import com.jd.dev.mock.server.interceptor.SessionInterception;
import com.jd.dev.mock.server.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *  WebMvc配置类
 *
 * @author dongbanghui
 * @date 2018/07/18
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Autowired
    RedisService redisService;

    @Bean
    public SessionInterception getSessionInterception(){
        return new SessionInterception(redisService);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getSessionInterception())
                .excludePathPatterns("/static/**")
                .order(10)
        ;
    }
}
