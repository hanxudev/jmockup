package com.jd.dev.mock.server.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by hanxu3 on 2018/7/27.
 */
@RestController
@RequestMapping("/join")
public class AppJoinController {

    /**
     * 接入页面
     * @return
     */
    @RequestMapping("/")
    public ModelAndView index(){
        return new ModelAndView("views/join/index");
    }
}
