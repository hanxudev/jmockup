package com.jd.dev.mock.server.service;

import com.jd.dev.mock.server.dao.mongodb.MockJSFDataRepository;
import com.jd.dev.mock.server.domain.MockJSFData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JsfDataService {

    @Autowired
    MockJSFDataRepository mockJSFDataRepository;

    /**
     * 条件分页查询
     * @param pageRequest
     * @param type
     * @param erp
     */
    public Page<MockJSFData> queryMockJsfDataByCondition(PageRequest pageRequest, String type, String erp) {
        Page<MockJSFData> pageMockJsfData = null;
        if(StringUtils.isNotBlank(type) && StringUtils.isBlank(erp)){
            pageMockJsfData = this.mockJSFDataRepository.findByTypeLike(type, pageRequest);
        }
        if(StringUtils.isNotBlank(erp) && StringUtils.isBlank(type)){
            pageMockJsfData = this.mockJSFDataRepository.findByErpLike(type, pageRequest);
        }
        if(StringUtils.isNotBlank(erp) && StringUtils.isNotBlank(type)){
            pageMockJsfData = this.mockJSFDataRepository.findByTypeAndErpLike(type, erp, pageRequest);
        }
        if(StringUtils.isBlank(erp) && StringUtils.isBlank(type)){
            pageMockJsfData = this.mockJSFDataRepository.findAll(pageRequest);
        }
        return pageMockJsfData;
    }

    /**
     * 根据id更新状态信息
     * @param id
     * @param status
     */
    public void updateStatusById(String id, Integer status) {

        Optional<MockJSFData> optional = this.mockJSFDataRepository.findById(id);

        optional.get().setStatus(status);

        this.mockJSFDataRepository.save(optional.get());

    }

    public void updateHttpDataById(MockJSFData mockJSFData) {

        Optional<MockJSFData> optional = this.mockJSFDataRepository.findById(mockJSFData.getId());

        if(mockJSFData.getMethodDesc()!=null){
            optional.get().setMethodDesc(mockJSFData.getMethodDesc());
        }

        if(mockJSFData.getResultData()!=null){
            optional.get().setResultData(mockJSFData.getResultData());
        }

        this.mockJSFDataRepository.save(optional.get());
    }

    /**
     * 全部数量
     * @return
     */
    public long getDataNum(){
        return this.mockJSFDataRepository.count();
    }
}
