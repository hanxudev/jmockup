package com.jd.dev.mock.server.controller;

import com.jd.dev.mock.server.bean.ResultBean;
import com.jd.dev.mock.server.dao.mongodb.MockJSFDataRepository;
import com.jd.dev.mock.server.service.JsfDataService;
import com.jd.dev.mock.server.utils.SessionUtil;
import com.jd.dev.mock.server.domain.MockJSFData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 *  JsfData数据
 *
 * @author dongbanghui
 * @date 2018/07/18
 */
@RestController
@RequestMapping("/jsf/data")
public class JsfDataController {

    @Autowired
    MockJSFDataRepository mockJSFDataRepository;

    @Autowired
    JsfDataService jsfDataService;

    /**
     * 进入jsf数据查询页面
     * @return
     */
    @RequestMapping("/query")
    public ModelAndView quewryPage(){
        return new ModelAndView("views/jsf/jsf_form_data");
    }

    /**
     * 分页查询jsfData数据
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/queryMockJsfDataByPage")
    public ResultBean queryMockJsfDataByPage(int page, int limit, HttpServletRequest request){
        PageRequest pageRequest = new PageRequest(page-1, limit);
        String erp = SessionUtil.getErpFromSession(request);
        Page<MockJSFData> pageMockHttpData = mockJSFDataRepository.findByErpLike(erp,pageRequest);
        ResultBean resultBean = new ResultBean(0,"", pageMockHttpData.getTotalElements(), pageMockHttpData.getContent());
        return resultBean;
    }

    /**
     * 根据条件查询jsfData数据
     * @param page
     * @param limit
     * @param type
     * @param erp
     * @return
     */
    @RequestMapping("/queryMockJsfDataByCondition")
    public ResultBean queryMockHttpDataByCondition(int page, int limit, String type, String erp){
        PageRequest pageRequest = new PageRequest(page-1, limit);
        Page<MockJSFData> pageMockJsfData = jsfDataService.queryMockJsfDataByCondition(pageRequest, type, erp);
        ResultBean resultBean = new ResultBean(0,"", pageMockJsfData.getTotalElements(), pageMockJsfData.getContent());
        return resultBean;
    }

    /**
     * 进入编辑页面
     * @param id
     * @return
     */
    @RequestMapping("/queryDetailForEdit")
    public ModelAndView queryDetail(String id){
        ModelAndView mv = new ModelAndView();
        mv.addObject("id", id);
        mv.setViewName("views/jsf/jsf_data_edit");
        return mv;
    }

    /**
     * 根据id获取jsfpData值
     * @param id
     * @return
     */
    @RequestMapping("/queryJsfDataById")
    public MockJSFData queryJsfDataById (String id){
        Optional<MockJSFData> operation = mockJSFDataRepository.findById(id);
        return operation.get();
    }

    /**
     * 根据id更新httpData数据
     * @param mockJSFData
     * @return
     */
    @PutMapping("/updateJsfDataById")
    public String updateHttpDataById(MockJSFData mockJSFData){
        this.jsfDataService.updateHttpDataById(mockJSFData);
        return "success";
    }

    /**
     * 根据id删除Jsf数据
     * @param id
     * @return
     */
    @DeleteMapping("/deleteJsfDataById")
    public String deleteJsfDataById(String id){
        this.mockJSFDataRepository.deleteById(id);
        return "success";
    }

    /**
     * 根据id更新状态信息
     * @param id
     * @return
     */
    @PutMapping("/updateStatusById")
    public String updateStatusById(String id, Integer status){
        this.jsfDataService.updateStatusById(id,  status);
        return "success";
    }

}
