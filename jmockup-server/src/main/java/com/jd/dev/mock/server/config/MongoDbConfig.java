package com.jd.dev.mock.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by hanxu3 on 2018/7/27.
 */
@Configuration
@PropertySource(value = "classpath:important.properties")
public class MongoDbConfig {
}
