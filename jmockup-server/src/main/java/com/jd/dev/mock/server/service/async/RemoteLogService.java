package com.jd.dev.mock.server.service.async;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jd.dev.mock.server.dao.mongodb.JsfLogRepository;
import com.jd.dev.mock.server.dao.mongodb.HttpLogRepository;
import com.jd.dev.mock.server.dao.mongodb.WebRequestLogRepository;
import com.jd.dev.mock.server.domain.log.HttpLogData;
import com.jd.dev.mock.server.domain.log.JsfLogData;
import com.jd.dev.mock.server.utils.RedisConstant;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.*;

/**
 *
 * 记录jsf/http调用信息
 * Created by hanxu3 on 2018/7/19.
 */
@Service
@Async
public class RemoteLogService {

    private static final Logger logger = LoggerFactory.getLogger(RemoteLogService.class);
    private static final Logger webLog = LoggerFactory.getLogger("WebRequest");

    private static final List<String> logPathList = Arrays.asList("/","/index","/main","/overview/console");

    @Autowired
    JsfLogRepository jsfLogRepository;

    @Autowired
    HttpLogRepository httpLogRepository;

    @Autowired
    WebRequestLogRepository webRequestLogRepository;

    @Autowired
    RedisTemplate<String,String> redisTemplate;

    /**
     * 记录请求日志
     * @param startTime 请求开始时间
     * @param request 原始请求
     * @param method 匹配的spring mvc 方法
     */
    public void logWebRequest(long startTime,HttpServletRequest request,Method method){
        //获取被拦截的方法名
        String methodName = method.getName();
        //获取所有请求参数key和value
        String keyValue = getReqParameter(request);
        //可以使用startTime匹配返回值
        webLog.info("{}请求url = {}", startTime,request.getRequestURL().toString());
        webLog.info("{}请求类型Method = {}", startTime, request.getMethod());
        String uri = request.getRequestURI();
        webLog.info("{}请求资源uri = {}", startTime, uri);
        webLog.info("{}请求参数 key：value = {}", startTime, keyValue);
        webLog.info("{}请求映射方法:{}", startTime, method.getDeclaringClass() + "." + methodName + "()");


        if(logPathList.contains(uri)){
            //特殊记录保存到redis
            //以天维度统计
            try {
                long time = startTime / DateUtils.MILLIS_PER_DAY;
                String redisKey = "pv.count.".concat(uri).concat(".").concat(String.valueOf(time));

                String value = redisTemplate.opsForValue().get(redisKey);
                if(StringUtils.isBlank(value)){
                    //新数据
                    //加入到set中
                    redisTemplate.opsForZSet().add(RedisConstant.KEY_PV_CPUNT_SET,redisKey,time);
                }
                redisTemplate.opsForValue().increment(redisKey,1);
            }catch (Exception e){
                logger.error("logWebRequest error:",e);
            }

//            try{
//                PageView pageView = new PageView();
//                pageView.setPath(uri);
//                //时间做个冗余，方便查询
//                pageView.setTime(System.currentTimeMillis());
//                pageView.setCreateDate(new Date());
//                pageView.setErp(SessionUtil.getErpFromSession(request));
//                webRequestLogRepository.save(pageView);
//            }catch (Exception e){
//                logger.error("save web log error:",e);
//            }
        }

    }

    /**
     * 获取json格式参数
     * @param request
     * @return
     */
    public String getReqParameter(HttpServletRequest request) {
        if (request == null) {
            return null;
        }
        Enumeration<String> enumeration = request.getParameterNames();
        JSONArray jsonArray = new JSONArray();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            String value = request.getParameter(key);
            JSONObject json = new JSONObject();
            //String keyValue = key+" : " +value+" ; ";
            json.put(key, value);
            jsonArray.add(json);
        }
        return jsonArray.toString();
    }

    /**
     * 记录jsf请求信息
     * @param logData
     */
    public void logJsfRequest(JsfLogData logData){
        incrReqCount();
        if(StringUtils.isNotBlank(logData.getResult())){
            //按接口清理数据
            if(StringUtils.isNotBlank(logData.getMethodKey())){
                jsfLogRepository.deleteAllByMethodKeyEquals(logData.getMethodKey());
            }
            jsfLogRepository.save(logData);
        }
    }

    /**
     * 记录http请求信息
     * @param logData
     */
    public void logHttpRequest(HttpLogData logData){
        incrReqCount();
        if(StringUtils.isNotBlank(logData.getResult())){
            //按接口清理数据
            if(StringUtils.isNotBlank(logData.getMethodKey())){
                httpLogRepository.deleteAllByMethodKeyEquals(logData.getMethodKey());
            }
            httpLogRepository.save(logData);
        }
    }

    private void incrReqCount(){
        try{
            //先检查有没有key,没有的话设置key的过期时间
            boolean hasKey = redisTemplate.hasKey(RedisConstant.KEY_REQ_COUNT);
            redisTemplate.opsForValue().increment(RedisConstant.KEY_REQ_COUNT,1);
            if(!hasKey){
                //以天为维度，0点过期
                Calendar calendar = Calendar.getInstance();
                calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH) + 1,0,0,0);
                Date expireDate = calendar.getTime();
                logger.info(RedisConstant.KEY_REQ_COUNT + " expireAt :"+expireDate.getTime());
                redisTemplate.expireAt(RedisConstant.KEY_REQ_COUNT,expireDate);
            }
        }catch (Exception e){
            logger.error("redis incrReqCount error:",e);
        }

    }

    private void decrReqCount(){
        try{
            redisTemplate.opsForValue().increment(RedisConstant.KEY_REQ_COUNT,-1);
        }catch (Exception e){
            logger.error("redis incrReqCount error:",e);
        }
    }
}
