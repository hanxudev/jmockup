package com.jd.dev.mock.server.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * User: wangyulie
 * Date: 16/8/18
 * Time: 11:24
 * Email: wangyulie@jd.com
 * http://git.jd.com/spring-boot/jd-spring-boot.git
 */
@ConfigurationProperties(prefix = "jd.jsf.registry")
public class JsfRegistryConfigProperties extends AbstractProperties{
    private static final String DEFAULT_ID = "jsfRegistry";
    private String id = DEFAULT_ID;
    private String address;
    private Integer batchCheck;
    private Integer connectTimeout;
    private Boolean subscribe;
    private String protocol;
    private String file;
    private String index;
    private Boolean register;
    private Integer timeout;
    private Map<String, String> parameters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getBatchCheck() {
        return batchCheck;
    }

    public void setBatchCheck(Integer batchCheck) {
        this.batchCheck = batchCheck;
    }

    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Integer connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public Boolean getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Boolean subscribe) {
        this.subscribe = subscribe;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public Boolean getRegister() {
        return register;
    }

    public void setRegister(Boolean register) {
        this.register = register;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}
