package com.jd.dev.mock.server.service;

import com.jd.dev.mock.server.domain.ApplicationData;
import com.jd.dev.mock.server.dao.mongodb.ApplicationDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by hanxu3 on 2018/8/13.
 */
@Service
public class AppTokenService {
    private static final Logger logger = LoggerFactory.getLogger(AppTokenService.class);
    @Autowired
    ApplicationDataRepository applicationDataRepository;

    /**
     * 根据token查erp
     * @return
     */
    public String getErpByToken(String token){
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnorePaths("ip")
                .withIgnorePaths("appName")
                .withIgnorePaths("appNameCn")
                .withIgnorePaths("types")
                .withIgnorePaths("erp")
                .withIgnoreNullValues();

        ApplicationData queryData = new ApplicationData();
        queryData.setToken(token);
        Example<ApplicationData> example = Example.of(queryData,matcher);
        try{
            Optional<ApplicationData> resOpt = applicationDataRepository.findOne(example);
            if(resOpt.isPresent()){
                ApplicationData res = resOpt.get();
                return res.getErp();
            }
        }catch (Exception e){
            logger.error("getErpByToken error:",e);
        }
        return "";
    }

    /**
     * 获取应用总数
     * @return
     */
    public long getTotalCount(){
        return applicationDataRepository.count();
    }
}
