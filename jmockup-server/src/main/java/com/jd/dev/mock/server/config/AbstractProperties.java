package com.jd.dev.mock.server.config;

/**
 * Created by hanxu3 on 2018/3/1.
 */
public class AbstractProperties {
    private boolean enabled = true;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
