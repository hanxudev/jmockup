package com.jd.dev.mock.server.domain.log;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * 用户页面浏览记录
 * Created by hanxu3 on 2018/9/3.
 */
@Document(collection = "mocker.log.pv")
public class PageView {
    /**
     * 路径
     */
    private String path;

    /**
     * 时间
     */
    private long time;

    private Date createDate;

    /**
     * 登录用户
     */
    private String erp;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getErp() {
        return erp;
    }

    public void setErp(String erp) {
        this.erp = erp;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
