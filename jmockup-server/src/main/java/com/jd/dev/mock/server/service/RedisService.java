package com.jd.dev.mock.server.service;

import com.jd.cap.data.api.model.XContainer;
import com.jd.dev.mock.server.utils.DateFormatConstant;
import com.jd.dev.mock.server.utils.RedisConstant;
import com.jd.fastjson.JSON;
import com.jd.jone.api.v2.beans.sysapp.AppWithSysInfo;
import com.jd.dev.mock.server.domain.UserBehavior;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by hanxu3 on 2018/8/7.
 */
@Service
public class RedisService {
    private static final Logger logger = LoggerFactory.getLogger(RedisService.class);
    private final String KEY_CAP_IP = "cap_ip_";

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    public XContainer getCapDataByIp(String ip) {
        String tempKey = KEY_CAP_IP.concat(ip);
        XContainer data = null;
        try {
            String json = redisTemplate.opsForValue().get(tempKey);
            data = JSON.parseObject(json, XContainer.class);
        } catch (Exception e) {
            logger.error("getCapDataByIp error:", e);
        }
        return data;
    }

    public void putCapDataByIp(String ip, XContainer data) {
        String tempKey = KEY_CAP_IP.concat(ip);
        try {
            String json = JSON.toJSONString(data);
            redisTemplate.opsForValue().set(tempKey, json, 1, TimeUnit.DAYS);
        } catch (Exception e) {
            logger.error("putCapDataByIp error:", e);
        }
    }


    public List<AppWithSysInfo> getAppInfoByErp(String applicationKey) {
        List<AppWithSysInfo> appWithSysInfoList = null;
        try {
            String json = redisTemplate.opsForValue().get(applicationKey);
            if (StringUtils.isNotEmpty(json)) {
                appWithSysInfoList = JSON.parseArray(json, AppWithSysInfo.class);
            }
        } catch (Exception e) {
            logger.error("getAppInfoByErp error:", e);
        }
        return appWithSysInfoList;
    }

    public void putAppInfo(String applicationKey, List<AppWithSysInfo> appWithSysInfos) {
        try {
            String json = JSON.toJSONString(appWithSysInfos);
            redisTemplate.opsForValue().set(applicationKey, json, 1, TimeUnit.DAYS);
        } catch (Exception e) {
            logger.error("putAppInfo error:", e);
        }
    }

    /**
     * 获取请求数
     * @return
     */
    public long getReqCount(){
        try{
            String value = redisTemplate.opsForValue().get(RedisConstant.KEY_REQ_COUNT);
            if(StringUtils.isNotBlank(value)){
                return Long.parseLong(value);
            }
        }catch (Exception e){
            logger.error("redis incrReqCount error:",e);
        }
        return 0;
    }

    /**
     * 记录用户登录动态
     * @param user
     */
    public void setLoginUser(String user){
        UserBehavior ub = new UserBehavior();
        ub.setName(user);
        long time = System.currentTimeMillis();
        ub.setTime(time);
        ub.setTimeDate(DateFormatUtils.format(time, DateFormatConstant.DEFAULT_DATE));
        try{
            redisTemplate.opsForList().leftPush(RedisConstant.KEY_LOGIN_USER, JSON.toJSONString(ub));
            long size = redisTemplate.opsForList().size(RedisConstant.KEY_LOGIN_USER);
            if(size > RedisConstant.KEY_USER_LOG_NUM){
                redisTemplate.opsForList().rightPop(RedisConstant.KEY_LOGIN_USER);
            }
        }catch (Exception e){
            logger.error("setLoginUser redis error:",e);
        }
    }

    /**
     * 登录用户列表
     * @return
     */
    public List<UserBehavior> getLoginUsers(){
        List<UserBehavior> users = new ArrayList<>();
        try{
            List<String> userDataList = redisTemplate.opsForList().range(RedisConstant.KEY_LOGIN_USER,0,RedisConstant.KEY_USER_LOG_NUM);
            if(CollectionUtils.isNotEmpty(userDataList)){
                for(String json : userDataList){
                    UserBehavior user = JSON.parseObject(json,UserBehavior.class);
                    users.add(user);
                }
            }
        }catch (Exception e){
            logger.error("getLoginUsers error:",e);
        }
        return users;
    }

    /**
     * 获取近几天的pv数据
     */
    public List<String> getPvDataList(long dayNum){
        long end = System.currentTimeMillis() / DateUtils.MILLIS_PER_DAY;
        long start = end - dayNum;
        Set<String> rangeWithScores = redisTemplate.opsForZSet().rangeByScore(RedisConstant.KEY_PV_CPUNT_SET,start,end);
        Iterator<String> iterator = rangeWithScores.iterator();
        List<String> keyList = new ArrayList<>();
        while(iterator.hasNext()) {
            String next = iterator.next();
            keyList.add(next);
        }

        return keyList;
    }

    /**
     * pv数据
     * @param dataKey
     * @return
     */
    public long getPvData(String dataKey){
        long start = System.currentTimeMillis() / DateUtils.MILLIS_PER_DAY;
        long end = start -1;
        return Long.parseLong(redisTemplate.opsForValue().get(dataKey));
    }
}
