package com.jd.dev.mock.server.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by hanxu3 on 2018/7/11.
 */
public class MockBaseData implements Serializable{
    //数据类型-http/jsf
    private String type;
    //创建人
    private String erp;
    //最后修改时间
    private Date modifyDate;
    //http接口是URL，jsf接口是class+method
    private String methodName;
    //接口描述
    private String methodDesc;
    //针对pin生效
    private String pin;
    //针对ip生效
    private String ip;
    //mock的接口返回数据
    private String resultData;
    //生效状态：0-未生效 1-生效
    private int status;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getErp() {
        return erp;
    }

    public void setErp(String erp) {
        this.erp = erp;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getMethodDesc() {
        return methodDesc;
    }

    public void setMethodDesc(String methodDesc) {
        this.methodDesc = methodDesc;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
