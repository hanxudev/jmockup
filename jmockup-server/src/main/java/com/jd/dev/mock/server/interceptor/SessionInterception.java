package com.jd.dev.mock.server.interceptor;

import com.jd.common.web.LoginContext;
import com.jd.dev.mock.server.utils.SessionUtil;
import com.jd.dev.mock.server.service.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionInterception implements HandlerInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(SessionInterception.class);

    RedisService redisService;

    public SessionInterception(RedisService redisService){
        this.redisService = redisService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取用户pin
        try {
            HttpSession httpSession = request.getSession(false);
            if(httpSession == null){
                httpSession = request.getSession();
            }
            if(httpSession != null
                    && httpSession.getAttribute(SessionUtil.KEY_NAME_USER) == null
                    && LoginContext.getLoginContext() != null){
                String pin = LoginContext.getLoginContext().getPin();
                String name = LoginContext.getLoginContext().getNick();
                logger.info("set loginUser:"+pin);
                httpSession.setAttribute(SessionUtil.KEY_NAME_USER,name);
                httpSession.setAttribute(SessionUtil.KEY_NAME_ERP,pin);

                if(redisService != null){
                    redisService.setLoginUser(pin);
                }
            }
        }catch (Exception e){
            logger.error("set loginUser error:",e);
        }
        return true;
    }
}
