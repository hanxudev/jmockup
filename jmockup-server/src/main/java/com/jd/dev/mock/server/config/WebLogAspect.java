package com.jd.dev.mock.server.config;

import com.alibaba.fastjson.JSON;
import com.jd.dev.mock.server.service.async.RemoteLogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 拦截记录controller请求日志
 */
@Aspect
@Configurable
@Component
public class WebLogAspect {
    private static final Logger logger = LoggerFactory.getLogger("WebRequest");
    private static final ThreadLocal<Long> timeTreadLocal = new ThreadLocal<>();

    @Autowired
    RemoteLogService remoteLogService;

    @Pointcut("execution(* com.jd.m.mocker.server..*.*(..)) && @annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void requestLog() {
    }

    @Before("requestLog()")
    public void before(JoinPoint joinPoint) {
        long startTime = System.currentTimeMillis();
        timeTreadLocal.set(startTime);
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //获取请求的request
        HttpServletRequest request = attributes.getRequest();

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        //获取被拦截的方法
        Method method = methodSignature.getMethod();
        //记录日志
        remoteLogService.logWebRequest(startTime,request,method);
    }

    @After("requestLog()")
    public void after() {
    }

    //controller请求结束返回时调用
    @AfterReturning(returning = "result", pointcut = "requestLog()")
    public Object afterReturn(Object result) {
        long startTime = timeTreadLocal.get();
        logger.info("{}返回值result = {}", startTime, JSON.toJSONString(result));
        double callTime = (System.currentTimeMillis() - startTime) / 1000.0;
        logger.info("{}花费时间time = {}s",startTime, callTime);
        return result;
    }
}