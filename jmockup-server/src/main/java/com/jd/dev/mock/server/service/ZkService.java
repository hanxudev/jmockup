package com.jd.dev.mock.server.service;

import com.jd.dev.mock.server.zk.ClientFactory;
import com.jd.dev.mock.server.zk.ZkTreeNode;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by hanxu3 on 2018/8/23.
 */
@Service
public class ZkService {
    private static final Logger logger = LoggerFactory.getLogger(ZkService.class);

    @Value("${zk.beta.connection}")
    private String zkConnectionString;
    private CuratorFramework betaClient = null;
    public static final String MULTI_WATCH_PATH = ";";

    @PostConstruct
    private void init(){
        if(betaClient == null){
            betaClient = ClientFactory.getBetaInstance(zkConnectionString);
        }
    }

    @PreDestroy
    private void close(){
        if(betaClient != null){
            betaClient.close();
        }
    }

    /**
     * 查询zk的数据
     * @param zkConnection
     * @param watchPath
     * @return
     */
    public ZkTreeNode getZkTreeData(String zkConnection, String watchPath){
        if(StringUtils.isNotBlank(zkConnection)){
            CuratorFramework tempClient = ClientFactory.newClient(zkConnection);
            tempClient.start();
            ZkTreeNode zkTreeNode = null;
            try{
                zkTreeNode = getZkTreeData(tempClient,watchPath);
            }catch (Exception e){
            }finally {
                if(tempClient != null){
                    tempClient.close();
                }
            }
            return zkTreeNode;
        }
        return getZkTreeData(betaClient,watchPath);
    }

    /**
     * 获取树状数据
     * @param watchPath
     * @return
     */
    private ZkTreeNode getZkTreeData(CuratorFramework client,String watchPath){
        ZkTreeNode treeRoot = new ZkTreeNode();
        treeRoot.setNodeName("root");
        treeRoot.setPath(watchPath);

        try {
            Set<String> watchPathArr = convertString2Set(watchPath);
            for(String path : watchPathArr){
                loadZkTreeData(client,path,null,treeRoot);
            }
        } catch (Exception e) {
            logger.error("loadZkTreeData error:",e);
        }
        return treeRoot;
    }

    /**
     * 递归查询数据
     * @param client
     * @param basePath
     * @param nodeName
     * @param root
     */
    public void loadZkTreeData(CuratorFramework client,String basePath,String nodeName,ZkTreeNode root){
        String newPath = basePath;
        //一个path其实就是一个node
        if(StringUtils.isNotBlank(nodeName)){
            newPath = basePath.concat("/").concat(nodeName);
        }
        ZkTreeNode treeNode = new ZkTreeNode();
        treeNode.setNodeName(nodeName);
        treeNode.setPath(newPath);
        root.addChild(treeNode);

        try{
            if(client.checkExists().forPath(newPath) == null){
                return;
            }
            List<String> childrenPath = client.getChildren().forPath(newPath);
            if(CollectionUtils.isNotEmpty(childrenPath)){
                for(String nodePath : childrenPath){
                    loadZkTreeData(client,newPath,nodePath,treeNode);
                }
            }else{
                NodeCache node = new NodeCache(client, newPath);
                node.start(true); //这个参数要给true  不然下边空指针...
                String value = new String(node.getCurrentData().getData() == null ? new byte[]{} : node.getCurrentData().getData());
                treeNode.setNodeValue(value);
            }
        }catch (Exception e){
            logger.error("loadZkData error:path={}",newPath,e);
        }

    }

    /**
     * 转换watchPath
     * @param watchPath
     * @return
     */
    private Set<String> convertString2Set(String watchPath) {
        Set<String> set = new LinkedHashSet<String>();
        if (StringUtils.isNotBlank(watchPath)) {
            String[] arr = watchPath.split(MULTI_WATCH_PATH);
            for (String it : arr) {
                if (StringUtils.isBlank(it)) {
                    continue;
                }
                set.add(it);
            }
        }
        return set;
    }

}
