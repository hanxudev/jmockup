package com.jd.dev.mock.server.dao.mongodb;

import com.jd.dev.mock.server.domain.log.JsfLogData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by hanxu3 on 2017/8/28.
 */
@Repository
public interface JsfLogRepository extends MongoRepository<JsfLogData,String> {
    /**
     * 按照methodKey删除
     * @param methodKey
     */
    void deleteAllByMethodKeyEquals(String methodKey);
}
