package com.jd.dev.mock.server.apiImpl;

import com.caucho.services.server.ServiceContext;
import com.jd.dev.mock.server.service.AppTokenService;
import org.springframework.remoting.caucho.HessianServiceExporter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Override HessianServiceExporter 增加ServiceContext.begin调用
 * 方便后期取ip、request、response等信息
 * Created by hanxu3 on 2018/7/30.
 */
public class MyHessianServiceExporter extends HessianServiceExporter {

    private AppTokenService appTokenService;

    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //没有找到ServiceContext.begin调用方式，暂时重写了HessianServiceExporter主动调用
        ServiceContext.begin(request,response,null,null);
        if(appTokenService != null){
            String token = request.getHeader("token");
            String erp = appTokenService.getErpByToken(token);
            ServiceContext.getContext().addHeader("erp",erp);
        }
        super.handleRequest(request, response);
    }

    public void setAppTokenService(AppTokenService appTokenService) {
        this.appTokenService = appTokenService;
    }
}
