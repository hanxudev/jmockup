package com.jd.dev.mock.server.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jd.dev.mock.server.dao.mongodb.EasyMockDataRepository;
import com.jd.dev.mock.server.domain.EasyMockData;
import com.jd.dev.mock.server.utils.SessionUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * easyMock功能
 * Created by liukun15 on 2018/8/8.
 */
@RestController
@RequestMapping("/mock/easymock")
public class EasyMockController {

    private static final Logger logger = LoggerFactory.getLogger(EasyMockController.class);
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    private static final String DEFAULT_VISIT_URL = "/mock/easymock/getUserResult?id=";
    private static final String PRIFIX_VISIT_URL = "http://";

    @Autowired
    EasyMockDataRepository easyMockDataRepository;

    /**
     * 获取首页
     * @return
     */
    @RequestMapping(value = "/getPage")
    public ModelAndView getEasyMockData() {
        return new ModelAndView("views/easymock/easymock_data");
    }

    /**
     * 获取首页数据
     * @return
     */
    @RequestMapping(value = "/getAll")
    public Map<String, Object> getAllDatas() {
        Map<String, Object> result = new HashMap<>();
        try {
            Sort sort = new Sort(Sort.Direction.DESC, "createTime");
            List<EasyMockData> all = easyMockDataRepository.findAll(sort);
            result.put("data", all);
            result.put("count", all.size());
            result.put("code", 0);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 删除EasyMock数据
     * @param id
     * @return
     */
    @RequestMapping(value = "/del")
    public int delEasyMockData(String id) {
        int result = OpreationStatus.failure.getStatusValue();
        try {
            easyMockDataRepository.deleteById(id);
            result = OpreationStatus.success.getStatusValue();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 跳转到edit页面，保留一个id
     * @param id
     * @return
     */
    @RequestMapping(value = "/todetails")
    public ModelAndView toEasyMockDetails(String id) {
        ModelAndView mv = new ModelAndView();
        if (StringUtils.isNotEmpty(id)) {
            mv.addObject("id", id);
        }
        mv.setViewName("views/easymock/easymock_edit");
        return mv;
    }

    /**
     * 获取详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/getdetails")
    public EasyMockData getEasyMockDetails(String id) {
        if(StringUtils.isBlank(id)){
            return null;
        }
        Optional<EasyMockData> optional = easyMockDataRepository.findById(id);
        if(optional.isPresent()){
            return optional.get();
        }
        return null;
    }


    /**
     * 编辑easyMock数据，保存更新数据
     * @param easyMockData
     * @return
     */
    @RequestMapping(value = "/updataEasyMock")
    public int updataEasyMock(EasyMockData easyMockData, HttpServletRequest request) {
        int result = OpreationStatus.failure.getStatusValue();
        try {
            if (StringUtils.isEmpty(easyMockData.getMockId())) {
                if(StringUtils.isNotEmpty(easyMockData.getEasyMockDesc())
                        && StringUtils.isNotEmpty(easyMockData.getResultData())){
                    EasyMockData easyMockDataByEasyMockDesc = easyMockDataRepository.findEasyMockDataByEasyMockDesc(easyMockData.getEasyMockDesc());
                    if(easyMockDataByEasyMockDesc != null){
                        return result;
                    }
                    String mockId = UUID.randomUUID().toString().replaceAll("-", "").concat(new Random().nextInt(1000) + "");
                    easyMockData.setMockId(mockId);
                    easyMockData.setErp(SessionUtil.getErpFromSession(request));
                    easyMockData.setCreateTime(simpleDateFormat.format(new Date()));
                    easyMockData.setUrl(PRIFIX_VISIT_URL + request.getServerName()+":"+request.getServerPort()+DEFAULT_VISIT_URL + mockId);
                    easyMockDataRepository.save(easyMockData);
                    result = OpreationStatus.success.getStatusValue();
                }
                return result;
            }
            Optional<EasyMockData> optional = easyMockDataRepository.findById(easyMockData.getMockId());
            if (optional.isPresent()) {
                EasyMockData oldData = optional.get();
                BeanUtils.copyProperties(easyMockData, oldData);
                oldData.setCreateTime(simpleDateFormat.format(new Date()));
                easyMockDataRepository.save(oldData);
                result = OpreationStatus.success.getStatusValue();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;

    }

    /**
     * 根据url查询出列表
     * @param page
     * @param limit
     * @param easyMockDesc
     * @return
     */
    @RequestMapping(value = "/queryEasyMockByDesc")
    public Map<String, Object> queryEasyMockByDesc(String page, String limit, String easyMockDesc) {
        Map<String, Object> result = new HashMap<>();
        Sort sort = new Sort(Sort.Direction.DESC, "createTime");
        int pageNum = StringUtils.isBlank(page) ? 0 : Integer.parseInt(page) - 1;
        int pageSize = StringUtils.isBlank(limit) ? 10 : Integer.parseInt(limit);
        Pageable pageable = new PageRequest(pageNum, pageSize, sort);
        try {
            EasyMockData easyMockData = new EasyMockData();
            ExampleMatcher matcher = ExampleMatcher.matchingAny()
                    .withMatcher("easyMockDesc", ExampleMatcher.GenericPropertyMatchers.contains())
                    .withIgnoreCase(true)//忽略大小写
                    .withIgnoreNullValues();
            easyMockData.setEasyMockDesc(easyMockDesc);
            Example<EasyMockData> example = Example.of(easyMockData, matcher);
            Page<EasyMockData> pages = easyMockDataRepository.findAll(example, pageable);
            result.put("data", pages.getContent());
            result.put("count", pages.getTotalElements());
            result.put("code", 0);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }


    /**
     * 读取文件判断.txt/json格式，回显到json编辑框
     * @param file
     * @param response
     * @return
     */
    @RequestMapping(value = "/upload")
    public Object uploadImg(MultipartFile file, HttpServletResponse response) {
        int result = OpreationStatus.failure.getStatusValue();
        try {
            String fileName = file.getOriginalFilename();
            String sufiixOfFile = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (!(sufiixOfFile.equals("txt") || sufiixOfFile.equals("json"))) {
                return result;
            }
            JSONObject resObj = new JSONObject();
            StringBuilder sb = new StringBuilder();
            String line;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line + "\r\n");
            }
            resObj.put("data", JSON.parse(sb.toString()));
            return resObj;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 用户请求url，获取result的数据
     * @param id
     * @return
     */
    @RequestMapping(value = "/getUserResult")
    public Object getUserDataByUrl(String id) {
        int result = OpreationStatus.failure.getStatusValue();
        try {
            Optional<EasyMockData> easyMockData = easyMockDataRepository.findById(id);
            if (StringUtils.isNotEmpty(easyMockData.get().getResultData())) {
                return JSON.toJSON(easyMockData.get().getResultData());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 常量
     */
    enum OpreationStatus {
        success(1), failure(0);
        int statusValue;

        OpreationStatus(int status) {
            this.statusValue = status;
        }

        public int getStatusValue() {
            return statusValue;
        }
    }

}
