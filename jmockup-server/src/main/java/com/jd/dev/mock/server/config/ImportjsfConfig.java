package com.jd.dev.mock.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by liukun15 on 2018/8/1.
 */
@Configuration
@ImportResource(locations = {"classpath:importjsf.xml"})
public class ImportjsfConfig {
}
