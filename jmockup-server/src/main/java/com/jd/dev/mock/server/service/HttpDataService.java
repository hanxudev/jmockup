package com.jd.dev.mock.server.service;

import com.jd.dev.mock.server.dao.mongodb.MockHttpDataRepository;
import com.jd.dev.mock.server.domain.MockHttpData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class HttpDataService {

    @Autowired
    MockHttpDataRepository mockHttpDataRepository;

    /**
     * 条件分页查询
     * @param pageRequest
     * @param type
     * @param erp
     */
    public  Page<MockHttpData> queryMockHttpDataByCondition(PageRequest pageRequest, String type, String erp) {
        Page<MockHttpData> pageMockHttpData = null;
        if(StringUtils.isNotBlank(type) && StringUtils.isBlank(erp)){
            pageMockHttpData = this.mockHttpDataRepository.findByTypeLike(type, pageRequest);
        }
        if(StringUtils.isNotBlank(erp) && StringUtils.isBlank(type)){
            pageMockHttpData = this.mockHttpDataRepository.findByErpLike(type, pageRequest);
        }
        if(StringUtils.isNotBlank(erp) && StringUtils.isNotBlank(type)){
            pageMockHttpData = this.mockHttpDataRepository.findByTypeAndErpLike(type, erp, pageRequest);
        }
        if(StringUtils.isBlank(erp) && StringUtils.isBlank(type)){
            pageMockHttpData = this.mockHttpDataRepository.findAll(pageRequest);
        }
        return pageMockHttpData;
    }

    /**
     * 按方法名erp查询
     * @param methodName
     * @param erp
     * @return
     */
    public MockHttpData queryHttpMockData(String methodName,String erp){
        try{
            return mockHttpDataRepository.findFirstByMethodNameAndErpAndStatus(methodName,erp,1);
        }catch (Exception e){

        }
        return null;
    }

    /**
     * 根据id更新状态信息
     * @param id
     * @param status
     */
    public void updateStatusById(String id, Integer status) {

        Optional<MockHttpData> optional = this.mockHttpDataRepository.findById(id);

        optional.get().setStatus(status);

        this.mockHttpDataRepository.save(optional.get());

    }

    /**
     * 根据id更新http数据
     * @param mockHttpData
     */
    public void updateHttpDataById(MockHttpData mockHttpData) {
        Optional<MockHttpData> optional = this.mockHttpDataRepository.findById(mockHttpData.getId());

        if(mockHttpData.getMethodDesc()!=null){
            optional.get().setMethodDesc(mockHttpData.getMethodDesc());
        }

        if(mockHttpData.getResultData()!=null){
            optional.get().setResultData(mockHttpData.getResultData());
        }

        this.mockHttpDataRepository.save(optional.get());

    }

    /**
     * 全部数量
     * @return
     */
    public long getDataNum(){
        return this.mockHttpDataRepository.count();
    }
}
