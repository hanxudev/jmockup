package com.jd.dev.mock.server.zk;

import java.util.ArrayList;
import java.util.List;

/**
 * zk的树形结构
 * Created by hanxu3 on 2018/8/22.
 */
public class ZkTreeNode {
    private String nodeName;
    private String nodeValue;
    //全路径
    private String path;
    private List<ZkTreeNode> children = new ArrayList<>();

    public void addChild(ZkTreeNode node){
        children.add(node);
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeValue() {
        return nodeValue;
    }

    public void setNodeValue(String nodeValue) {
        this.nodeValue = nodeValue;
    }

    public List<ZkTreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<ZkTreeNode> children) {
        this.children = children;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
