package com.jd.dev.mock.server.controller;

import com.jd.dev.mock.server.service.MenuDataService;
import com.jd.dev.mock.server.domain.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by hanxu3 on 2018/7/6.
 */
@RestController
@RequestMapping("/menu")
public class MenuController {
    private static final Logger logger = LoggerFactory.getLogger(HttpFormController.class);

    @Autowired
    MenuDataService menuDataService;

    /**
     * 查询菜单内容
     * @return
     */
    @RequestMapping("/data")
    public List<Menu> getAllData(){
        return menuDataService.getMenuData();
    }

    /**
     * 添加一个菜单
     * @param menu
     * @return
     */
    @RequestMapping("/add")
    public String addMenu(@RequestBody Menu menu){
        try{
            menuDataService.addMenuData(menu);
        }catch (Exception e){
            logger.error("addMenuData error:",e);
        }
        return "success";
    }

    /**
     * 删除一个菜单
     * @param menu
     * @return
     */
    @RequestMapping("/del")
    public String delMenu(@RequestBody Menu menu){
        menuDataService.delMenuData(menu);
        return "success";
    }
}
