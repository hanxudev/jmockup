package com.jd.dev.mock.server.apiImpl;

import com.caucho.services.server.ServiceContext;
import com.jd.cap.data.api.model.Page;
import com.jd.cap.data.api.model.XContainer;
import com.jd.cap.data.api.model.XQContainer;
import com.jd.cap.data.api.service.XCMDBService;
import com.jd.dev.mock.server.service.MockerInvokeService;
import com.jd.fastjson.JSON;
import com.jd.dev.mock.common.api.JsfServiceAdapter;
import com.jd.dev.mock.common.jsf.JsfServiceInfo;
import com.jd.dev.mock.common.jsf.RequestExtend;
import com.jd.dev.mock.server.service.JsfInvokeService;
import com.jd.dev.mock.server.service.RedisService;
import com.jd.dev.mock.server.utils.IP4Utils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by hanxu3 on 2018/5/14.
 */
@Service("jsfServiceAdapter")
public class JsfServiceAdapterImpl implements JsfServiceAdapter {

    private static final Logger logger = LoggerFactory.getLogger(JsfServiceAdapterImpl.class);
    @Autowired
    JsfInvokeService jsfInvokeService;
    @Autowired
    MockerInvokeService mockerInvokeService;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    XCMDBService xcmdbService;

    @Autowired
    RedisService redisService;

    @Override
    public Object doJsfRequest(JsfServiceInfo invocation, RequestExtend extend) {
        //增加ip的校验
        HttpServletRequest servletRequest = (HttpServletRequest) ServiceContext
                .getContextRequest();

        String requestIp = IP4Utils.getIpAddr(servletRequest);
        logger.info("requestIp:"+requestIp+" invocation:["+JSON.toJSONString(invocation)+"]*****extend:["+JSON.toJSONString(extend)+"]");
        //调用mocker平台，如果有mock数据则直接返回，如果没有则调用真实jsf接口
        Object mockData = mockerInvokeService.invokeMocker(invocation, extend);
        if(null != mockData){
            logger.info("mockData:["+JSON.toJSONString(mockData)+"]");
            return mockData;
        }
        return jsfInvokeService.invokeJsf(invocation);
    }

    /**
     * 校验是否为预发布或者测试ip
     * @param ip
     * @return
     */
    @Override
    public boolean checkIp(String ip) {
        logger.info("checkIp:"+ip);
        //先查询缓存
        XContainer data = redisService.getCapDataByIp(ip);
        if(data != null){
            return data.getEnvId() == 5;
        }

        XQContainer query = new XQContainer();
        query.setIp(ip);
        Page<XContainer> resData = xcmdbService.findByQuery(query,1,1);
        if(resData == null){
            //调用接口失败，默认false
            return false;
        }
        if(resData.getPagination().getTotalRecord() == 0){
            //没有查到记录默认true
            return true;
        }
        if(CollectionUtils.isNotEmpty(resData.getResult())){
            List<XContainer> datas = resData.getResult();
            data = datas.get(0);
            //存一份缓存
            redisService.putCapDataByIp(ip,data);
            if(data != null && data.getEnvId() == 5){
                return true;
            }
        }
        return false;
    }
}
