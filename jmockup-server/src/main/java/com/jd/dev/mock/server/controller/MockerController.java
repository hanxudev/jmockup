package com.jd.dev.mock.server.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jd.dev.mock.common.mock.MockerBaseInfo;
import com.jd.dev.mock.common.mock.MockerRequest;
import com.jd.dev.mock.common.mock.MockerResponse;
import com.jd.dev.mock.common.tools.RegexUtils;
import com.jd.dev.mock.server.dao.mongodb.MockJSFDataRepository;
import com.jd.dev.mock.server.domain.MockHttpData;
import com.jd.dev.mock.server.domain.MockMethodData;
import com.jd.dev.mock.server.service.AppTokenService;
import com.jd.dev.mock.server.service.JsfInvokeService;
import com.jd.dev.mock.server.utils.IP4Utils;
import com.jd.dev.mock.server.utils.SessionUtil;
import com.jd.dev.mock.server.domain.MockJSFData;
import com.jd.dev.mock.server.dto.MethodMockResponse;
import com.jd.dev.mock.server.dto.MethodQueryRequest;
import com.jd.dev.mock.server.service.MockMethodService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 提供APi接口
 * Created by hanxu3 on 2018/5/11.
 */
@RestController
@RequestMapping("/mocker")
public class MockerController {
    private static final Logger logger = LoggerFactory.getLogger(MockerController.class);
    private static final String DEFAULT_LIST_TYPE = "java.util.List";

    @Autowired
    JsfInvokeService jsfInvokeService;

    @Autowired
    MockMethodService mockMethodService;
    @Autowired
    MockJSFDataRepository mockJSFDataRepository;

    @Autowired
    AppTokenService appTokenService;


    @RequestMapping("/jsfpage")
    public ModelAndView jsfPage() {
        return new ModelAndView("views/jsf_form");
    }

    @RequestMapping("/test")
    public ModelAndView test() {
        return new ModelAndView("views/test");
    }

    @RequestMapping("/method/create")
    public ModelAndView methodAddView() {
        return new ModelAndView("views/method/add");
    }

    @RequestMapping("/method/list")
    public ModelAndView methodListView() {
        return new ModelAndView("views/method/list");
    }

    @RequestMapping("/method/edit")
    public ModelAndView methodEditView(@RequestParam("id") String id) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("id", id);
        mv.setViewName("views/method/edit");
        return mv;
    }

    @ResponseBody
    @RequestMapping("/method/data")
    public MethodMockResponse mockMethod(@RequestParam(name = "token") String token,
                                         @RequestParam(name = "localClassName")  String localClassName,
                                         @RequestParam(name = "localMethodName")  String localMethodName,
                                         @RequestParam(name = "args")   List<String> args){
        MethodMockResponse response=new MethodMockResponse();

        String erp  = appTokenService.getErpByToken(token);
        MethodQueryRequest methodQueryRequest=new MethodQueryRequest(erp,localClassName,localMethodName,args);
        MockMethodData mockMethodData=mockMethodService.mockData(methodQueryRequest);
        if(mockMethodData==null){
            response.setStatus("exception");
            response.setMessage("未匹配到数据");
        }else{
            response.setStatus("success");
            if(!StringUtils.isEmpty(mockMethodData.getReturnType())){
                response.setReturnType(mockMethodData.getReturnType());
            }
            response.setReturnData(mockMethodData.getResultData());
        }
        logger.info("mkLog ==> 返回数据：" + response.toString());
        return response;
    }
    /**
     * 根据id查询出某条数据
     * @param id
     * @return
     */
    @RequestMapping("/getDataById")
    public Map<String,Object> getMockJSFDataById(String id) {
        Map<String,Object> result  = new HashMap<>();
        if(StringUtils.isEmpty(id)){
            result.put("errorMsg","id is null");
            result.put("code",-1);
            return result;
        }
        MockJSFData mockJSFData = mockJSFDataRepository.findMockJSFDataById(id.trim());
        result.put("data",mockJSFData);
        result.put("code",0);
        return result;

    }

    /**
     * 获取某个方法的所有mock数据
     *
     * @param mockJSFData
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/getAllByMethod")
    public Map<String, Object> getMockData(String page, String limit, MockJSFData mockJSFData) {
        Map<String, Object> result = new HashMap<>(3);
        if (StringUtils.isBlank(mockJSFData.getErp())) {
            //erp为空设置NULL值使mongo查询取消匹配
            mockJSFData.setErp(null);
        }
        if (StringUtils.isEmpty(mockJSFData.getMethodName())) {
            mockJSFData.setMethodName(null);
        }
        int pageNum = StringUtils.isBlank(page) ? 0 : Integer.parseInt(page) - 1;
        int pageSize = StringUtils.isBlank(limit) ? 10 : Integer.parseInt(limit);
        Sort sort = new Sort(Sort.Direction.DESC, "modifyDate");
        Pageable pageable = new PageRequest(pageNum, pageSize, sort);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("methodName", ExampleMatcher.GenericPropertyMatchers.contains())
                .withIgnorePaths("modifyDate")
                .withIgnorePaths("resultData")
                .withIgnorePaths("status")
                .withIgnoreCase(true)//忽略大小写
                .withIgnoreNullValues();

        Example<MockJSFData> example = Example.of(mockJSFData, matcher);
        Page pageData = mockJSFDataRepository.findAll(example, pageable);
        List<MockHttpData> datas = pageData.getContent();
        result.put("code", 0);
        result.put("data", datas);
        result.put("count", pageData.getTotalElements());
        return result;
    }



    /**
     * 模糊匹配线上接口，自动填充表单
     *
     * @param search
     * @return
     */
    @RequestMapping("/autoGetjsfData")
    public Map<String, Object> getAutoCompleteData(String search) {
        ExampleMatcher matcher = ExampleMatcher.matchingAny()
                .withMatcher("methodName", ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("desc", ExampleMatcher.GenericPropertyMatchers.contains())
                .withIgnoreCase(true)//忽略大小写
                .withIgnorePaths("status")
                .withIgnoreNullValues();
        MockJSFData mockJSFData = new MockJSFData();
        mockJSFData.setMethodName(search);
        Example<MockJSFData> example = Example.of(mockJSFData, matcher);
        List<MockJSFData> datas = mockJSFDataRepository.findAll(example);
//        for (MockJSFData jsfData : datas) {
//            jsfData.setMethodName(jsfData.getMethodName().substring(0, jsfData.getMethodName().lastIndexOf(".")));
//        }
//        Set<MockJSFData> dataSet = new HashSet<>(datas);
        Map<String, Object> result = new HashMap<>();
        result.put("data", datas);
        return result;
    }

    /**
     * 保存mock数据
     *
     * @param mockerRequest
     * @return
     */
    @RequestMapping("/savemock")
    public MockJSFData saveMock(MockerRequest mockerRequest, HttpServletRequest request) {
        if (mockerRequest == null) {
            logger.error("mockerRequest is null");
            return null;
        }
        MockJSFData mockJSFData = new MockJSFData();
        String erp = SessionUtil.getErpFromSession(request);
        mockJSFData.setErp(erp);
        mockJSFData.setResultData(mockerRequest.getBaseInfo().getResultData());
        mockJSFData.setType("jsf");
        mockJSFData.setStatus(0);
        mockJSFData.setModifyDate(new Date());
        mockJSFData.setIp(IP4Utils.getIpAddr(request));
        if (mockerRequest.getBaseInfo() != null ) {
            if(StringUtils.isNotEmpty(mockerRequest.getBaseInfo().getMethodDesc())){
                mockJSFData.setMethodDesc(mockerRequest.getBaseInfo().getMethodDesc());
            }
            if(StringUtils.isNotEmpty(mockerRequest.getBaseInfo().getArgsType())){
                mockJSFData.setArgsType(mockerRequest.getBaseInfo().getArgsType());
            }
            if(StringUtils.isNotEmpty(mockerRequest.getBaseInfo().getArgs())){
                mockJSFData.setArgs(mockerRequest.getBaseInfo().getArgs());
            }
        }
        if (mockerRequest.getJsfServiceInfo() != null) {
            if(StringUtils.isNotEmpty(mockerRequest.getJsfServiceInfo().getToken())){
                mockJSFData.setToken(mockerRequest.getJsfServiceInfo().getToken());
            }
            if(StringUtils.isNotEmpty(mockerRequest.getJsfServiceInfo().getAlias())){
                mockJSFData.setAlias(mockerRequest.getJsfServiceInfo().getAlias());
            }
            if (StringUtils.isNotEmpty(mockerRequest.getJsfServiceInfo().getReturnType())) {
                mockJSFData.setReturnType(mockerRequest.getJsfServiceInfo().getReturnType());
            }
            if (StringUtils.isNotEmpty(mockerRequest.getJsfServiceInfo().getClazzName())
                    && StringUtils.isNotEmpty(mockerRequest.getJsfServiceInfo().getMethodName())) {
                //jsf接口是class+method,连接方式用“.”
                mockJSFData.setMethodName(mockerRequest.getJsfServiceInfo().getClazzName().concat(".")
                        .concat(mockerRequest.getJsfServiceInfo().getMethodName()));
            }
        }
        return mockJSFDataRepository.save(mockJSFData);
    }

    /**
     * 处理jsf请求
     *
     * @param mockerRequest
     * @return
     */
    @RequestMapping("/jsf")
    public Object jsfServiceInvoke(MockerRequest mockerRequest) {
        MockerResponse response = new MockerResponse();
        if (mockerRequest == null || mockerRequest.getJsfServiceInfo() == null) {
            response.setErrorMsg("jsf param is null");
            return response;
        }
        //基本配置
        MockerBaseInfo baseInfo = mockerRequest.getBaseInfo();
        setArgsAndType(response, baseInfo, mockerRequest);

        //解析token
        parseTokenJson(mockerRequest);

        Object jsfReturn = jsfInvokeService.invokeJsf(mockerRequest.getJsfServiceInfo());
        if (jsfReturn != null) {
            return jsfReturn;
        } else {
            logger.info("Jsf service get null result{}", JSON.toJSONString(mockerRequest.getJsfServiceInfo()));
        }

        return response;
    }

    /**
     * 解析token的json字符串
     * @param mockerRequest
     */
    private void parseTokenJson(MockerRequest mockerRequest){
        String token = mockerRequest.getJsfServiceInfo().getToken();
        if(StringUtils.isNotBlank(token)){
            Map<String,String> tokenMap = new HashMap();
            if(RegexUtils.isJSONString(token)){
                try{
                    JSONObject jsonObject = JSON.parseObject(token);
                    Map<String,String> tempMap = jsonObject.toJavaObject(Map.class);
                    if(!tempMap.isEmpty()){
                        for(Map.Entry<String,String> entry : tempMap.entrySet()){
                            String key = entry.getKey();
                            tokenMap.put(key,entry.getValue());
                        }
                    }
                }catch (Exception e){
                }
            }else{
                tokenMap.put("token",token);
            }
            mockerRequest.getJsfServiceInfo().setParameters(tokenMap);
        }
    }

    /**
     * 根据基础数据转换json数组为对应的jsf参数
     *
     * @param response
     * @param baseInfo
     * @param mockerRequest
     */
    private void setArgsAndType(MockerResponse response, MockerBaseInfo baseInfo, MockerRequest mockerRequest) {
        if (baseInfo == null && baseInfo.getArgs() == null) {
            response.setResult("baseInfo is null");
            return;
        }
        String args = baseInfo.getArgs().trim();
        JSONArray params;
        try {
            params = JSONArray.parseArray(args);
        } catch (Exception e) {
            logger.error(e.getMessage(), e.getCause());
            response.setErrorMsg(e.getMessage());
            return;
        }
        int paramsLength = params == null ? 1 : params.size();
        String[] realTypes = new String[paramsLength];
        String userType = baseInfo.getArgsType();
        if (StringUtils.isNotBlank(userType)) {
            String[] types = userType.split(",");
            if (types != null) {
                for (int j = 0; j < types.length; j++) {
                    realTypes[j] = types[j];
                }
            }
        }
        Object[] realArgs = new Object[paramsLength];
        getRealArgs(params, realArgs, realTypes);
        //参数类型设置
        mockerRequest.getJsfServiceInfo().setArgs(realArgs);
        mockerRequest.getJsfServiceInfo().setArgsType(realTypes);
    }

    /**
     * 获取最终传递的参数
     *
     * @param params
     * @param realArgs
     * @param realTypes
     */
    private void getRealArgs(JSONArray params, Object[] realArgs, String[] realTypes) {
        for (int i = 0; i < params.size(); i++) {
            //基本类型
            Object value = params.get(i);
            if (value instanceof JSONArray) {
                //数组或者集合参数的处理
                if(StringUtils.isBlank(realTypes[i])){
                        realTypes[i] = DEFAULT_LIST_TYPE;
                    }
                JSONArray jsonArray = (JSONArray)value;
//                JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(value));
//                for (Object object:jsonArray.toArray()){
//                    if(object instanceof JSONObject){
//                        //如果有对象那么做集合处理
//                        realTypes[i]=DEFAULT_LIST_TYPE;
//                        break;
//                    } else{
//                        //否则当做数组来处理
//                        Object generalEelement = Array.newInstance(object.getClass(), jsonArray.size());
//                        realTypes[i]= generalEelement.getClass().getName();
//                        break;
//                    }
//                }
                realArgs[i] = parseArray(jsonArray);
            } else if (value instanceof JSONObject) {
                //JavaBean参数的处理
                JSONObject temp = (JSONObject) value;
                realTypes[i] = temp.getString("class");
                //调jsf接口：javabean转成map
                realArgs[i] = parseMapValue(temp);
            } else {
                //基本类型参数处理
                realArgs[i] = value;
                String clzName = value.getClass().getName();
                if (StringUtils.isBlank(realTypes[i])) {
                    realTypes[i] = clzName;
                }
            }
        }
    }

    /**
     * 递归处理map转化
     *
     * @param value
     * @return
     */
    private Object parseMapValue(JSONObject value) {
        Map<String, Object> mapValue = JSON.parseObject(value.toJSONString(), HashMap.class);
        for (Map.Entry<String, Object> entry : mapValue.entrySet()) {
            Object fieldValue = entry.getValue();
            if (fieldValue instanceof JSONArray) {
                List<Object> list = new ArrayList<>();
                for (Object object : ((JSONArray) fieldValue).toArray()) {
                    publicParse(object, list);
                }
                mapValue.put(entry.getKey(), list);
            } else if (fieldValue instanceof JSONObject) {
                mapValue.put(entry.getKey(), parseMapValue((JSONObject) fieldValue));
            } else {
                mapValue.put(entry.getKey(), fieldValue);
            }
        }
        return mapValue;
    }

    /**
     * 解析数组或者集合
     *
     * @param jsonArray
     * @return
     */
    private Object parseArray(JSONArray jsonArray) {
//        JSONArray jsonArray = JSONArray.parseArray((JSON.toJSONString(object)));
        List<Object> list = new ArrayList<>();
        for (Object jsonValue : jsonArray.toArray()) {
//            List<Object> innerList = new ArrayList<>();
            publicParse(jsonValue, list);
//            list.add(innerList);
        }
        return list;
    }

    /**
     * 转换时候的公共代码部分
     *
     * @param jsonValue
     * @param innerList
     */
    private void publicParse(Object jsonValue, List<Object> innerList) {
        if (jsonValue instanceof JSONArray) {
            innerList.add(parseArray((JSONArray) jsonValue));
        } else if (jsonValue instanceof JSONObject) {
            innerList.add(parseMapValue((JSONObject) jsonValue));
        } else {
            innerList.add(jsonValue);
        }
    }

    /**
     * 获取远端访问的ip地址
     *
     * @param request
     * @return
     */
    private String getRemoteIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
}
