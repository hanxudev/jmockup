package com.jd.dev.mock.server.service;

import com.jd.fastjson.JSON;
import org.apache.commons.collections.MapUtils;
import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by hanxu3 on 2017/8/30.
 */
@Service
public class HttpClientService {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientService.class);

    HttpClientContext context = HttpClientContext.create();
    @Autowired
    CloseableHttpClient httpclient;

    public String postRequest(String requestUrl,Map<String,String> params,Map<String,String> headers,List<Cookie> cookies){

        HttpPost httpPost = new HttpPost(requestUrl);
        httpPost.addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0");
        if(headers != null){
            for(Map.Entry<String,String> header : headers.entrySet()){
                httpPost.addHeader(header.getKey(),header.getValue());
            }
        }

        if(cookies != null && cookies.size() > 0){
            for(Cookie cookie : cookies){
                context.getCookieStore().addCookie(cookie);
            }
        }

        if(MapUtils.isNotEmpty(params)){
            List<NameValuePair> formparams = new ArrayList<>();
            for(Map.Entry<String,String> entry : params.entrySet()){
                formparams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
            httpPost.setEntity(entity);
        }

        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        try {
            String response = httpclient.execute(httpPost,responseHandler,context);
            return response;
        } catch (IOException e) {
            logger.error("postRequest error:",e);
        }
        return null;
    }

    /**
     * Get请求
     * @param requestUrl
     * @param params
     * @param cookies
     * @return
     */
    public String sendGet(String requestUrl,Map<String,String> params,Map<String,String> headers,Map<String,String> cookies){

        if(cookies != null){
            for(Map.Entry<String,String> entry : cookies.entrySet()){
                BasicClientCookie cookie = new BasicClientCookie(entry.getKey(), entry.getValue());
                cookie.setDomain("");
                cookie.setPath("/");
                context.getCookieStore().addCookie(cookie);
            }
        }
        return sendGetwithCookies(requestUrl,params,headers,null);
    }

    /**
     * 携带cookies
     * @param requestUrl
     * @param params
     * @param headers
     * @param cookies
     * @return
     */
    public String sendGetwithCookies(String requestUrl,Map<String,String> params,Map<String,String> headers,List<Cookie> cookies){
        logger.error("http cookies:"+ JSON.toJSONString(cookies));
        HttpGet httpGet = new HttpGet(requestUrl);
        httpGet.addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0");

        if(headers != null){
            for(Map.Entry<String,String> header : headers.entrySet()){
                httpGet.addHeader(header.getKey(),header.getValue());
            }
        }

        if(cookies != null && cookies.size() > 0){
            for(Cookie cookie : cookies){
                context.getCookieStore().addCookie(cookie);
            }
        }

        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        try {
            logger.error("CookieStore :"+JSON.toJSONString(context.getCookieStore()));
            String response = httpclient.execute(httpGet,responseHandler,context);
            return response;
        } catch (IOException e) {
            logger.error("postRequest error:",e);
        }
        return null;
    }
}
