package com.jd.dev.mock.server.dao.mongodb;

import com.jd.dev.mock.server.domain.EasyMockData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * Created by liukun15 on 2018/8/8.
 */
public interface EasyMockDataRepository extends MongoRepository<EasyMockData,String> {
    @Query(value="{'easyMockDesc':?0}")
    EasyMockData findEasyMockDataByEasyMockDesc(String easyMockDesc);
}
