package com.jd.dev.mock.server.jsf;

import com.jd.jsf.gd.GenericService;
import com.jd.jsf.gd.config.ConsumerConfig;
import com.jd.jsf.gd.config.RegistryConfig;
import com.jd.jsf.gd.util.Constants;
import com.jd.jsf.gd.util.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * 管理jsf泛化调用生成的service
 * Created by hanxu3 on 2018/3/23.
 */
public class GenericServiceFactory {

    /**
     * 每个接口都会生成一个GenericService对象，这个map会比较大
     * key值是否应该考虑分组?todo
     */
    private static Map<String,GenericService> genericServiceMap = new ConcurrentHashMap<String,GenericService>();

    /**
     * 构建GenericService实例
     * @param jsfRegistry
     * @return
     */
    public static GenericService getGenericService(RegistryConfig jsfRegistry,String clazz,String alias,Map<String,String> parameters){
        String keyName = clazz.concat("_").concat(alias);
        GenericService genericService = genericServiceMap.get(keyName);
        if(genericService == null){
            // 服务提供者连接注册中心，设置属性
            ConsumerConfig<GenericService> consumerConfig = new ConsumerConfig<GenericService>();
            // 这里写真实的类名
            consumerConfig.setInterfaceId(clazz);
            consumerConfig.setRegistry(jsfRegistry);
            consumerConfig.setProtocol(Constants.DEFAULT_PROTOCOL);
            consumerConfig.setAlias(alias);
            consumerConfig.setGeneric(true); // 需要指定是Generic调用true
            if(parameters != null && !parameters.isEmpty()){
                for(Map.Entry<String,String> entry : parameters.entrySet()){
                    String key = entry.getKey();
                    if(!key.startsWith(".")){
                        key = ".".concat(key);
                    }
                    consumerConfig.setParameter(key,entry.getValue());
                }
            }
            genericService = consumerConfig.refer();
            genericServiceMap.put(keyName,genericService);
        }
        return genericService;
    }
}
