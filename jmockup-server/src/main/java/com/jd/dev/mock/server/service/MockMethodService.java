package com.jd.dev.mock.server.service;

import com.alibaba.fastjson.JSON;
import com.jd.dev.mock.common.exception.ServiceException;
import com.jd.dev.mock.common.mock.MockerBaseInfo;
import com.jd.dev.mock.common.mock.MockerMethodRequest;
import com.jd.dev.mock.server.domain.MockMethodData;
import com.jd.dev.mock.server.dto.MethodQueryRequest;
import com.jd.dev.mock.server.dao.mongodb.MockMethodRepository;
import com.jd.dev.mock.server.dto.MethodFormRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by zhangzhao8 on 2018/7/14.
 */
@Service
public class MockMethodService {
    @Autowired
    private MockMethodRepository mockMethodRepository;

    public MockMethodData getById(String  id){
       return  mockMethodRepository.findById(id);
    }
    public void save(MethodFormRequest formData){
        MockMethodData data=new MockMethodData();
        BeanUtils.copyProperties(formData,data);
        mockMethodRepository.save(data);
    }
    public void save(MethodFormRequest formData,String id){
        MockMethodData data=new MockMethodData();
        data.set_id(id);
        BeanUtils.copyProperties(formData, data);
        mockMethodRepository.save(data);
    }

    public void copy(String id){
        MockMethodData mockMethodData=mockMethodRepository.findById(id);
        mockMethodData.set_id(null);
        mockMethodData.setModifyDate(new Date());
        mockMethodRepository.save(mockMethodData);
    }

    public void delete(String id){
        mockMethodRepository.deleteById(id);
    }

    public Map<String,Object> findAll(MethodQueryRequest queryRequest, int page, int limit){
        Map resultMap=new HashMap<>();
        Sort sort = new Sort(Sort.Direction.DESC, "modifyDate");
        Query query=new Query().skip((page-1)*limit).limit(page*limit).with(sort);
        if(!StringUtils.isEmpty(queryRequest.getErp())){
            query.addCriteria(Criteria.where("erp").is(queryRequest.getErp()));
        }
        if(!StringUtils.isEmpty(queryRequest.getLocalClassName())){//mongodb模糊查询性能很差
            Pattern pattern = Pattern.compile("^.*" + queryRequest.getLocalClassName() + ".*$", Pattern.MULTILINE);
            query.addCriteria(Criteria.where("localClassName").regex(pattern));
        }
        if(!StringUtils.isEmpty(queryRequest.getLocalMethodName())){
            query.addCriteria(Criteria.where("localMethodName").is(queryRequest.getLocalMethodName()));
        }


        resultMap.put("data",mockMethodRepository.findAll(query));
        resultMap.put("count", mockMethodRepository.countByCondition(query));
        return resultMap;
    }

    public MockMethodData mockData(MethodQueryRequest queryRequest){
        Query query=new Query();
        Query bakQuery=new Query();//防止带有入参的查询查不到，返回不带入参的查询
        if(!StringUtils.isEmpty(queryRequest.getErp())){
            query.addCriteria(Criteria.where("erp").is(queryRequest.getErp()));
            bakQuery.addCriteria(Criteria.where("erp").is(queryRequest.getErp()));
        }
        if(!StringUtils.isEmpty(queryRequest.getLocalClassName())){//mongodb模糊查询性能很差
            query.addCriteria(Criteria.where("localClassName").is(queryRequest.getLocalClassName()));
            bakQuery.addCriteria(Criteria.where("localClassName").is(queryRequest.getLocalClassName()));
        }
        if(!StringUtils.isEmpty(queryRequest.getLocalMethodName())){
            query.addCriteria(Criteria.where("localMethodName").is(queryRequest.getLocalMethodName()));
            bakQuery.addCriteria(Criteria.where("localMethodName").is(queryRequest.getLocalMethodName()));
        }
        if(queryRequest.getArgs()!=null&&queryRequest.getArgs().size()>0){
            StringBuilder argsStr=new StringBuilder();
            queryRequest.getArgs().stream().forEach(arg->{
                argsStr.append(arg).append(",");
            });
            argsStr.deleteCharAt(argsStr.length()-1);
            query.addCriteria(Criteria.where("argsClassName").is(argsStr.toString()));
        }
        MockMethodData mockMethodData= mockMethodRepository.findOne(query);
        if(mockMethodData==null){
            mockMethodData= mockMethodRepository.findOne(bakQuery);
        }
        return mockMethodData;
    }

    public String handle(MockerMethodRequest request) throws ServiceException {
        MockerBaseInfo baseInfo=request.getBaseInfo();
        if(baseInfo!=null){
            String erp=baseInfo.getErp();
            String methodName=request.getClazz()+"."+request.getMethodName();
            MockMethodData data=null;
            if(data!=null){
                return JSON.toJSONString(data);
            }else{
                throw new ServiceException(200,"无此数据");
            }
        }else{
            throw new ServiceException(100,"参数传递错误");
        }
    }
}
