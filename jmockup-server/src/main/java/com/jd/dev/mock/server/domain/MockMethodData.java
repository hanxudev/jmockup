package com.jd.dev.mock.server.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


/**
 * Created by zhangzhao8 on 2018/7/14.
 */
@Document(collection = "mocker.data.method")
public class MockMethodData extends MockBaseData{

    //类型
    private String _id;
    //类型
    private String localClassName;
    //方法名
    private String localMethodName;
    //参数类名，多个以,分隔
    private String argsClassName;
    //返回实现类名
    private String returnType;

    public MockMethodData() {
        this.setType("method");
        this.setModifyDate(new Date());
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id=_id;
    }

    public String getLocalClassName() {
        return localClassName;
    }

    public void setLocalClassName(String localClassName) {
        this.localClassName=localClassName;
    }

    public String getLocalMethodName() {
        return localMethodName;
    }

    public void setLocalMethodName(String localMethodName) {
        this.localMethodName=localMethodName;
    }

    public String getArgsClassName() {
        return argsClassName;
    }

    public void setArgsClassName(String argsClassName) {
        this.argsClassName=argsClassName;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType=returnType;
    }
}
