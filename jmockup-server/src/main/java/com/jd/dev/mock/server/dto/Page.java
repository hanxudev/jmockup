package com.jd.dev.mock.server.dto;

/**
 * Created by zhangzhao8 on 2018/7/21.
 */
public class Page {
    //第几页
    private int pageNO=0;
    //每页条数
    private int pageSize=10;

    public int getPageNO() {
        return pageNO;
    }

    public void setPageNO(int pageNO) {
        this.pageNO=pageNO;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize=pageSize;
    }
}
