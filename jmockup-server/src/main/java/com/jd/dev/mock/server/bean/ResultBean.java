package com.jd.dev.mock.server.bean;

import java.util.List;

public class ResultBean {

	private int code;

	private  String msg;

	// 数据的总条数
    private Long count;

    // 当前显示的数据
    private List<?> data;

    public ResultBean(int code, String msg, Long count, List<?> data) {
    	this.code = code;
    	this.msg = msg;
        this.count = count;
        this.data = data;
    }

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}
}