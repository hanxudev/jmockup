package com.jd.dev.mock.server.dao.mongodb;

import com.jd.dev.mock.server.domain.log.PageView;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by hanxu3 on 2017/8/28.
 */
@Repository
public interface WebRequestLogRepository extends MongoRepository<PageView,String> {
    /**
     * 按照methodKey删除
     * @param start
     * @param end
     */
    long countByTimeBetween(long start, long end);

    long countByPathEqualsAndTimeBetween(String path,long start, long end);
}
