package com.jd.dev.mock.server.dao.mongodb.impl;

import com.jd.dev.mock.server.dao.mongodb.CustomMockMethodRepository;
import com.jd.dev.mock.server.domain.MockMethodData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by zhangzhao8 on 2018/7/23.
 */
@Component
public class MockMethodRepositoryImpl implements CustomMockMethodRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<MockMethodData> findAll(Query query) {
        return mongoTemplate.find(query, MockMethodData.class);
    }

    @Override
    public MockMethodData findOne(Query query) {
        return mongoTemplate.findOne(query,MockMethodData.class);
    }

    @Override
    public long countByCondition(Query query) {
        return mongoTemplate.count(query, MockMethodData.class);
    }
}
