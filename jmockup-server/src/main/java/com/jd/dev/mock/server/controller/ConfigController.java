package com.jd.dev.mock.server.controller;

import com.alibaba.fastjson.JSONObject;
import com.jd.dev.mock.common.ResultConstant;
import com.jd.dev.mock.server.service.ZkService;
import com.jd.dev.mock.server.zk.ZkTreeNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 配置接口
 * Created by hanxu3 on 2018/8/15.
 */
@RestController
@RequestMapping("mocker")
public class ConfigController {
    private static final Logger logger = LoggerFactory.getLogger(HttpFormController.class);

    private static final Map<String,String> configMap = new HashMap<>();
    static {
        configMap.put(ResultConstant.APP_ZK_HOST,"192.168.192.48:2181");
    }

    @Autowired
    ZkService zkService;

    @RequestMapping("/config")
    public Map<String,String>  getConfig(){
        return configMap;
    }

    @RequestMapping("/config/add")
    public Map<String,String>  addConfig(String json){
        if(StringUtils.isNotBlank(json)){
            try{
                HashMap<String,String> jsonMap = JSONObject.parseObject(json, HashMap.class);
                configMap.putAll(jsonMap);
            }catch (Exception e){
                logger.error("addConfig error:",e);
            }
        }
        return configMap;
    }

    /**
     * 拉取预发布zk配置
     * @return
     */
    @RequestMapping("/config/zk")
    public ZkTreeNode getZkConfig(String zkConnection,String watchPath){
        return zkService.getZkTreeData(zkConnection,watchPath);
    }
}
