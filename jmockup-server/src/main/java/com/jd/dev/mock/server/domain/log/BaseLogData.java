package com.jd.dev.mock.server.domain.log;

/**
 * 记录线上请求日志
 * Created by hanxu3 on 2018/7/19.
 */
public class BaseLogData {
    //唯一标识，同一个methodKey存一份数据
    private String methodKey;
    //服务名称
    private String methodName;
    //请求参数
    private String reqest;
    //返回结果
    private String result;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getReqest() {
        return reqest;
    }

    public void setReqest(String reqest) {
        this.reqest = reqest;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMethodKey() {
        return methodKey;
    }

    public void setMethodKey(String methodKey) {
        this.methodKey = methodKey;
    }
}
