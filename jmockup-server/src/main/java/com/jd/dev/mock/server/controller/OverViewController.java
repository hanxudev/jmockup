package com.jd.dev.mock.server.controller;

import com.jd.dev.mock.server.domain.OverView;
import com.jd.dev.mock.server.domain.PVCount;
import com.jd.dev.mock.server.service.AppTokenService;
import com.jd.dev.mock.server.service.HttpDataService;
import com.jd.dev.mock.server.service.JsfDataService;
import com.jd.dev.mock.server.service.RedisService;
import com.jd.dev.mock.server.utils.SessionUtil;
import com.jd.dev.mock.server.dao.mongodb.WebRequestLogRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 一些静态页面的访问
 * Created by hanxu3 on 2018/8/8.
 */
@RestController
@RequestMapping("/overview")
public class OverViewController {
    private static final Logger logger = LoggerFactory.getLogger(OverViewController.class);

    @Autowired
    AppTokenService appTokenService;

    @Autowired
    JsfDataService jsfDataService;

    @Autowired
    HttpDataService httpDataService;

    @Autowired
    RedisService redisService;

    @Autowired
    WebRequestLogRepository webRequestLogRepository;

    @RequestMapping("/{name}")
    public ModelAndView indexPage(@PathVariable String name,HttpServletRequest request){
        ModelAndView page = new ModelAndView("views/overview/"+name);
        page.addObject("ov",dataAppCount(request));
        return page;
    }

    @RequestMapping(value = "/data/manager")
    public ModelAndView dataManager(){
        return new ModelAndView("views/overview/data_manager");
    }

    /**
     * 统计数量
     * @return
     */
    @RequestMapping(value = "/data/count")
    public OverView dataAppCount(HttpServletRequest request){
        OverView overView = new OverView();
        //接入应用数
        long appTotalNum = appTokenService.getTotalCount();
        overView.setAppTotalNum(appTotalNum);
        overView.setAppNewNum(appTotalNum);
        long jsfDataNum = jsfDataService.getDataNum();
        long httpDataNum = httpDataService.getDataNum();
        overView.setInterfaceNum(jsfDataNum + httpDataNum);
        overView.setInterfaceNewNum(jsfDataNum + httpDataNum);

        overView.setPvNum(getPvNum());

        long reqCount = redisService.getReqCount();
        overView.setRequestNum(reqCount);
        overView.setJsfReqNum(reqCount);
        overView.setUserNum(2);
        //在线用户数
        overView.setUserDayNum(SessionUtil.getUserSessionNum(request));

        //用户动态
        overView.setUserList(redisService.getLoginUsers());

        return overView;
    }

    /**
     * pv数据
     * @return
     */
    @RequestMapping(value = "/data/pv")
    public List<PVCount> getPvCountData(){
        List<PVCount> datas = new ArrayList<>();

        Map<String,PVCount> cache = new HashMap<>();

        List<String> keys = redisService.getPvDataList(7);
        if(CollectionUtils.isNotEmpty(keys)){
            for(Object key : keys){
                String redisKey = (String) key;
                String day = getDayTagByKey(redisKey);
                long value = redisService.getPvData(redisKey);

                PVCount count = cache.get(day);
                if(count != null){
                    count.addCount(value);
                }else{
                    count = new PVCount();
                    count.setDate(day);
                    count.setCount(value);
                    datas.add(count);
                    cache.put(day,count);
                }
            }
        }
        return datas;
    }

    private String getDayTagByKey(String redisKey){
        int i = redisKey.lastIndexOf(".");
        String day = redisKey.substring(i+1);
        long time = Long.parseLong(day) * DateUtils.MILLIS_PER_DAY;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        int dayNum = calendar.get(Calendar.DAY_OF_MONTH);
        return String.valueOf(dayNum);
    }

    /**
     * 获取pv访问量
     * @return
     */
    private long getPvNum(){
        long pvNum = 0;
        try {
            Calendar start = Calendar.getInstance();
            start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH),
                    0, 0, 0);
            pvNum = webRequestLogRepository.countByTimeBetween(start.getTimeInMillis(),System.currentTimeMillis());
        }catch (Exception e){
            logger.error("getPvNum error:",e);
        }
        return pvNum;
    }
}
