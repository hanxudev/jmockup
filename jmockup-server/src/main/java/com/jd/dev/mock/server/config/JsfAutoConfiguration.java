package com.jd.dev.mock.server.config;

import com.jd.jsf.gd.config.RegistryConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * User: wangyulie
 * Date: 16/8/18
 * Time: 10:08
 * Email: wangyulie@jd.com
 * http://git.jd.com/spring-boot/jd-spring-boot.git
 */
@Configuration
@ConditionalOnClass(com.jd.jsf.gd.GenericService.class)
@EnableConfigurationProperties(JsfRegistryConfigProperties.class)
public class JsfAutoConfiguration {

    private final JsfRegistryConfigProperties jsfRegistryConfigProperties;

    public JsfAutoConfiguration(JsfRegistryConfigProperties jsfRegistryConfigProperties) {
        this.jsfRegistryConfigProperties = jsfRegistryConfigProperties;
    }

    @Bean(name = "jsfRegistry")
    @ConditionalOnMissingBean
    @ConditionalOnProperty(name = "jd.jsf.registry.enabled", matchIfMissing = false)
    public RegistryConfig registryConfig() {
        RegistryConfig registryConfig = new RegistryConfig();
        if (jsfRegistryConfigProperties.getId() != null) {
            registryConfig.setId(jsfRegistryConfigProperties.getId());
        }
        if (jsfRegistryConfigProperties.getAddress() != null) {
            registryConfig.setAddress(jsfRegistryConfigProperties.getAddress());
        }
        if (jsfRegistryConfigProperties.getBatchCheck() != null) {
            registryConfig.setBatchCheck(jsfRegistryConfigProperties.getBatchCheck());
        }
        if (jsfRegistryConfigProperties.getConnectTimeout() != null) {
            registryConfig.setConnectTimeout(jsfRegistryConfigProperties.getConnectTimeout());
        }
        if (jsfRegistryConfigProperties.getSubscribe() != null) {
            registryConfig.setSubscribe(jsfRegistryConfigProperties.getSubscribe());
        }
        if (jsfRegistryConfigProperties.getProtocol() != null) {
            registryConfig.setProtocol(jsfRegistryConfigProperties.getProtocol());
        }
        if (jsfRegistryConfigProperties.getFile() != null) {
            registryConfig.setFile(jsfRegistryConfigProperties.getFile());
        }
        if (jsfRegistryConfigProperties.getIndex() != null) {
            registryConfig.setIndex(jsfRegistryConfigProperties.getIndex());
        }
        if (jsfRegistryConfigProperties.getRegister() != null) {
            registryConfig.setRegister(jsfRegistryConfigProperties.getRegister());
        }
        if (jsfRegistryConfigProperties.getTimeout() != null) {
            registryConfig.setTimeout(jsfRegistryConfigProperties.getTimeout());
        }
        if (jsfRegistryConfigProperties.getParameters() != null) {
            registryConfig.setParameters(jsfRegistryConfigProperties.getParameters());
        }
        return registryConfig;
    }


}
