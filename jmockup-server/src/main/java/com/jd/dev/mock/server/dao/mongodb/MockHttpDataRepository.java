package com.jd.dev.mock.server.dao.mongodb;

import com.jd.dev.mock.server.domain.MockHttpData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by hanxu3 on 2017/8/28.
 */
@Repository
public interface MockHttpDataRepository extends MongoRepository<MockHttpData,String> {

    @Query(value="{'type':?0}")
    Page<MockHttpData> findByTypeLike(String type, Pageable pageable);

    @Query(value="{'erp':?0}")
    Page<MockHttpData> findByErpLike(String erp, Pageable pageable);

    @Query(value="{'type':?0,erp:?1}")
    Page<MockHttpData> findByTypeAndErpLike(String type, String erp,Pageable pageable);

    /**
     * 按方法名查询
     * @param method
     * @return
     */
    MockHttpData findFirstByMethodName(String method);

    /**
     * 根据方法名，erp查询
     * @param method
     * @param erp
     * @return
     */
    MockHttpData findFirstByMethodNameAndErpAndStatus(String method,String erp,int status);
}
