package com.jd.dev.mock.server.controller;

import com.jd.dev.mock.server.bean.ResultBean;
import com.jd.dev.mock.server.dao.mongodb.MockHttpDataRepository;
import com.jd.dev.mock.server.service.HttpDataService;
import com.jd.dev.mock.server.domain.MockHttpData;
import com.jd.dev.mock.server.utils.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 *  HttpData数据
 *
 * @author dongbanghui
 * @date 2018/07/18
 */
@RestController
@RequestMapping("/http/data")
public class HttpDataController {

    @Autowired
    MockHttpDataRepository mockHttpDataRepository;

    @Autowired
    HttpDataService httpDataService;

    /**
     * 进入httpData数据查询页面
     * @return
     */
    @RequestMapping("/query")
    public ModelAndView quewryPage(){
        return new ModelAndView("views/http/http_form_data");
    }

    /**
     * 分页查询httpData数据
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("/queryMockHttpDataByPage")
    public ResultBean queryMockHttpDataByPage(int page, int limit, HttpServletRequest request){
        PageRequest pageRequest = new PageRequest(page-1, limit);
        String erp = SessionUtil.getErpFromSession(request);
        Page<MockHttpData> pageMockHttpData = mockHttpDataRepository.findByErpLike(erp,pageRequest);
        ResultBean resultBean = new ResultBean(0,"", pageMockHttpData.getTotalElements(), pageMockHttpData.getContent());
        return resultBean;
    }

    /**
     * 根据条件查询httpData数据
     * @param page
     * @param limit
     * @param type
     * @param erp
     * @return
     */
    @RequestMapping("/queryMockHttpDataByCondition")
    public ResultBean queryMockHttpDataByCondition(int page, int limit, String type, String erp){
        PageRequest pageRequest = new PageRequest(page-1, limit);
        Page<MockHttpData> pageMockHttpData = httpDataService.queryMockHttpDataByCondition(pageRequest, type, erp);
        ResultBean resultBean = new ResultBean(0,"", pageMockHttpData.getTotalElements(), pageMockHttpData.getContent());
        return resultBean;
    }

    /**
     * 进入编辑页面
     * @param id
     * @return
     */
    @RequestMapping("/queryDetailForEdit")
    public ModelAndView queryDetail(String id){
        ModelAndView mv = new ModelAndView();
        mv.addObject("id", id);
        mv.setViewName("views/http/http_data_edit");
        return mv;
    }

    /**
     * 根据id获取httpData值
     * @param id
     * @return
     */
    @RequestMapping("/queryHttpDataById")
    public MockHttpData queryHttpDataById (String id){
        Optional<MockHttpData> operation = mockHttpDataRepository.findById(id);
        return operation.get();
    }

    /**
     * 根据id更新httpData数据
     * @param mockHttpData
     * @return
     */
    @PutMapping("/updateHttpDataById")
    public String updateHttpDataById(MockHttpData mockHttpData){
        this.httpDataService.updateHttpDataById(mockHttpData);
        return "success";
    }

    /**
     * 根据id删除http数据
     * @param id
     * @return
     */
    @DeleteMapping("/deleteHttpDataById")
    public String deleteHttpDataById(String id){
        this.mockHttpDataRepository.deleteById(id);
        return "success";
    }

    /**
     * 根据id修改状态信息
     * @param id
     * @return
     */
    @PutMapping("/updateStatusById")
    public String updateStatusById(String id,Integer status){
        this.httpDataService.updateStatusById(id, status);
        return "success";
    }

}
