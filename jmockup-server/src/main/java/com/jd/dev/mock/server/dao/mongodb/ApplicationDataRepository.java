package com.jd.dev.mock.server.dao.mongodb;

import com.jd.dev.mock.server.domain.ApplicationData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * Created by liukun15 on 2018/7/31.
 */
public interface ApplicationDataRepository extends MongoRepository<ApplicationData,Long> {
    @Query(value="{'erp':?0}")
    List<ApplicationData> findApplicationDataByErp(String erp);
}
