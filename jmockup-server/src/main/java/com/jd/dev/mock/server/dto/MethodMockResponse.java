package com.jd.dev.mock.server.dto;

/**
 * Created by zhangzhao8 on 2018/7/28.
 */
public class MethodMockResponse {
    private String status;
    private String message;
    private String returnType;
    private String returnData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status=status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message=message;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType=returnType;
    }

    public String getReturnData() {
        return returnData;
    }

    public void setReturnData(String returnData) {
        this.returnData=returnData;
    }
}
