package com.jd.dev.mock.server.service;

import com.jd.dev.mock.server.dao.mongodb.MenuRepository;
import com.jd.dev.mock.server.domain.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hanxu3 on 2018/7/6.
 */
@Service
public class MenuDataService {

    @Autowired
    MenuRepository menuRepository;

    public List<Menu> getMenuData(){
        return menuRepository.findAll();
    }

    /**
     * 添加
     * @param menu
     */
    public void addMenuData(Menu menu){
        menuRepository.save(menu);
    }

    /**
     * 删除
     * @param menu
     */
    public void delMenuData(Menu menu) {
        menuRepository.delete(menu);
    }
}
