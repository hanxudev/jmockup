package com.jd.dev.mock.server.domain;

/**
 * Created by hanxu3 on 2018/9/3.
 */
public class PVCount {
    private String date;
    private long count = 0;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public void addCount(long num){
        this.count += num;
    }
}
