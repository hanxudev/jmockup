package com.jd.dev.mock.server;

import com.jd.dev.mock.common.api.JsfServiceAdapter;
import com.jd.dev.mock.server.apiImpl.MyHessianServiceExporter;
import com.jd.dev.mock.server.service.AppTokenService;
import com.jd.spring.boot.autoconfigure.ump.UmpAutoConfiguration;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

@SpringBootApplication(
		exclude = {UmpAutoConfiguration.class}
)
@EnableConfigurationProperties
@EnableAsync
@EnableAspectJAutoProxy
@ServletComponentScan(basePackages = {"com.jd.m.mocker.server"})
@ComponentScan(basePackages = {"com.jd.m.mocker"})
public class MockerServerApplication  extends SpringBootServletInitializer{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MockerServerApplication.class);
	}

	/**
	 * 对外发布的hessian接口
	 */
	@Component
	public class CustomerService {
		@Autowired
		private JsfServiceAdapter jsfServiceAdapter;

		@Autowired
		private AppTokenService appTokenService;

		@Bean(name = "/hessian/JsfServiceAdapter")
		public HessianServiceExporter exportJsfService() {
			MyHessianServiceExporter exporter = new MyHessianServiceExporter();
			exporter.setService(jsfServiceAdapter);
			exporter.setServiceInterface(JsfServiceAdapter.class);
			exporter.setAppTokenService(appTokenService);
			exporter.afterPropertiesSet();
			return exporter;
		}
	}

	@Bean
	public CloseableHttpClient httpClient(){
		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
		connectionManager.setDefaultMaxPerRoute(200);
		connectionManager.setMaxTotal(800);

		CookieStore cookieStore = new BasicCookieStore();
		CloseableHttpClient httpclient = HttpClients.custom()
				.setDefaultCookieStore(cookieStore)
				.setConnectionManager(connectionManager)
				.setMaxConnTotal(600)
				.setMaxConnPerRoute(200)
				.build();
		return httpclient;
	}

	@Bean
	public Executor asyncExecutor(@Value("${task.pool.coreSize}") int coreSize,
								  @Value("${task.pool.maxSize}") int maxSize,
								  @Value("${task.pool.queueSize}") int queueSize) {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(coreSize);
		executor.setMaxPoolSize(maxSize);
		executor.setQueueCapacity(queueSize);
		executor.setThreadNamePrefix("Mocker-Async-");
		executor.initialize();
		return executor;
	}

	public static void main(String[] args) {
		SpringApplication.run(MockerServerApplication.class, args);
	}
}
